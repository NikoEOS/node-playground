//libraries
const express = require("express");
const expressStaticGzip = require("express-static-gzip");

//const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");

const { readdirSync } = require('fs')

//functions
const rebuild = require("./rebuild");

const app = express();

app.set('view engine', 'ejs');

//cors - not in use but for future reference
/*
var corsOptions = {
  origin: "front-end-ip cannot-be-*",
  credentials: true
};

app.use(cors(corsOptions));
*/

// (optional) only made for logging and
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.raw());

if (app.get("env") === "developement") {
  app.use(logger("dev"));
}

//test function
// !note! This overwrites call /test to wp
app.get("/test", function (req, res) {
  res.send("Hello fellow tester.");
});

app.get("/rebuild", function (req, res) {
  rebuild.rebuild();
  res.send(rebuild.getRebuildStatus());
});

app.get("/rebuild/status", function (req, res) {
  res.send(rebuild.getRebuildStatus());
});


/* === Static file server === */
app.use("/static", express.static("static"))

app.use("/", (req, res, next) => {
  if(req.url === "/"){
    const getDirectories = source =>
    readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)
    console.log(getDirectories("public"))
    res.render("pages/index",{apps:getDirectories("public")})
  }else{
    next()
  } 
}, express.static('public'));

//app.use(api.home, express.static(static_dir));  //to run without gzipping

module.exports = app;
