let game = {}
let data = {}
let defaultData = {
  rounds: [],
  cards: 54,
  serve: [10,9,8,7,6,5,4,3,2,1,1,2,3,4,5,6,7,8,9,10],
  players: []
}

game.save = () => {
  console.log(data)
  localStorage.setItem('gameData', JSON.stringify(data));
}

game.load = () => {
  let storedData = localStorage.getItem('gameData')
  if(storedData){
    data = JSON.parse(localStorage.getItem('gameData'));
    return data
  }else{
    data = defaultData
  }
  return false
}

game.reset = () => {
  localStorage.removeItem('gameData')
  game.load()
}

game.resume = () =>{
  console.log("resume")
}

game.players = (players) => {
  data.players = players
}
/*
let round = {
  number: 1,
  cards: 10,
  players: [
    {
      name: "p1",
      said: 1,
      got: true
    },
    {
      name: "p2",
      said: 1,
      got: true
    }
  ] 
}
*/
game.rounds = () => {
  return data.rounds
}

game.round = (number) => {
  console.log()
  if(number && number <= data.rounds.length){
    return data.rounds[number-1]
  }else{
    return game.generateRound()
  }
  
}

game.generateRound = () => {
  let round = {
    number: data.rounds.length + 1,
    cards: Math.min(data.serve[data.rounds.length], Math.floor(data.cards/data.players.length)),
    players: [],
    dealer: 0,
    isEnd: data.rounds.length == data.serve.length
  }

    
  data.players.forEach(player => {
    player = {
      name: player,
      said: 0,
      success: false
    }
    round.players.push(player)
  }) 

  round.dealer = ((data.rounds.length+2) % data.players.length)

  return round
}

game.score = (round) => {

  let rounds = data.rounds

  let scores
  let current = []

  let isHistory = round && round <= rounds.length

  console.log(isHistory)

  if(isHistory){
    rounds = rounds.slice(0, round)  
    console.log(rounds)
  }

  roundScores = (round) => {
    return round.players.map((player, index) => {
      return game.calculateScore(player.said, player.success)
    })
  }

  sumArray = (arr1, arr2) => {
    return arr1.map((value, index) => {
      return value + arr2[index]
    })
  }

  totalScores = (roundScores) => {
    return roundScores.reduce((acc, cur) => {
      return sumArray(acc, cur);
    });
  }



  if(rounds.length > 0){
    roundScoreMatrix = rounds.map(roundScores)
    console.log(roundScoreMatrix)
    scores = totalScores(roundScoreMatrix)
    if(isHistory){
      current = roundScoreMatrix[round-1]
      console.log(current)
    }
  }else{
    scores = data.players.map(() => {return 0})
  }

  return scores.map((score, index) => {
    return {name: data.players[index], score: score, current: current[index]}
  })

}

game.calculateScore = (said, success) => {
  if(success && said > 0){
    return (said + 5) * 2
  }else if(!success){
    return (said + 5) * -1
  }else{
    return 0
  }
}


