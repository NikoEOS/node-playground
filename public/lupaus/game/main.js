const urlParams = new URLSearchParams(window.location.search);
let form = $('form')
let round = {}

$(document).ready(init)

function init() {
  game.load()

  round = game.round(checkRound())

  makeScores()

  if (round.isEnd) {
    end()
  }

  round.players.forEach(element => {
    generatePointsField(element)
  });

  $("#round").append(" " + round.number)
  if(round.number <= game.rounds().length){
    $("#status").show()
  }
  $("#cards").append(": " + round.cards)
  $("#dealer").append(": " + round.players[round.dealer].name)

  generateRoundButtons()
  

}

function checkRound() {

 let roundNumber = parseInt(urlParams.get("round"))
 let roundAmount = game.rounds().length

 if(roundNumber < 1 || roundNumber > roundAmount){
  roundNumber = roundAmount + 1
  urlParams.set("round", roundNumber)
  window.history.replaceState({}, '', `${location.pathname}?${urlParams}`);
 }
 return roundNumber

}

form.submit(function(e) {
  e.preventDefault();

  let valid = true

  let totalSaid = round.players.map(e => {return e.said}).reduce((acc, cur) => {return acc + cur})

  if(totalSaid == round.cards){
    $("#error").text("Total promised cannot be equal to cards served")
    valid = false
  }
  console.log(totalSaid, round.cards)
  if (valid){
  
    if(round.number > game.rounds().length){
      data.rounds.push(round)
    }

    game.save()
    
    let roundNumber = parseInt(urlParams.get("round"))
    
    const url = new URL(window.location);
    url.searchParams.set("round", round.number);
    window.history.pushState({}, "", url);

    if(roundNumber){
      window.location.href = ".?round=" + (roundNumber + 1);
    }else {
      window.location.href = ".";
    }
  }
});

function generatePointsField(player){

  let playerContainer = document.createElement("div")

  let points = document.createElement("input")
  points.type = "number"
  points.value = player.said
  points.name = "" + player.name + "Points"
  points.setAttribute("class", "form-control")
  points.setAttribute("required", "true")

  $(points).change(() => {
    player.said = parseInt(points.value)
  })

  let success = document.createElement("input")
  success.type = "checkbox"
  success.checked = player.success
  success.name = player.name + "Success"
  success.setAttribute("class", "btn-check")
  success.setAttribute("id", success.name)
  let successLabel = document.createElement("label")
  successLabel.setAttribute("for", success.name)
  successLabel.setAttribute("class", "btn")
  successLabel.innerHTML = "happy"

  $(success).change(() => {
    player.success = success.checked
  })

  let name = document.createElement("p")
  name.innerHTML = player.name
  

  $(playerContainer).append(name, points, success, successLabel)
  $("#game #players").append(playerContainer)
}

function generateRoundButtons(){

  $("#prevRound").click(goPrevRound)

  if((urlParams.get('round') > 1 || !urlParams.get('round')) && data.rounds.length > 0){
    $("#prevRound").show()
  }
}




function goPrevRound(e) {
  e.preventDefault()
  game.save()
  window.location.href = "./?round=" + (round.number-1);
}

function makeScores() {
  let scores = $("#scores")
  game.score(round.number).forEach(score => {
    let div = document.createElement("div")
    $(div).text(score.name + ": " + score.score)
    if(score.current){
      $(div).append(" (" + score.current + ")")
    }
    scores.append(div)
  })
}

function end() {
  $("#game").hide()
  $("#prevRound").appendTo("#scores")
}
