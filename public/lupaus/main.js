let form = $('form')

$(document).ready(() => {
  init(game.load())
  addPlayer()
})

form.submit(function(e) {
  e.preventDefault();
  // Get all the forms elements and their values in one step
  
  let players = [];
  let valid = true;

  $(this).serializeArray().forEach(element => {
    query = "label[for=" + element.name + "]"
    if(element.value == "" || players.indexOf(element.value) != -1){
      
      $(query).attr("error", "Name needs to be unique")
      valid = false
    }else{
      $(query).removeAttr("error")
    }
    players.push(element.value)  
  });

  if(valid){
    game.reset()
    game.players(players)
    game.save()
    window.location.href = "./game";
  }
});

$('#addPlayer').click(addPlayer);

function init(data){
  let oldGame = $("#oldGame")
  if(!data){
    oldGame.hide()
  }
}

function addPlayer(){
  let players = $("form #players")
  
  generatePlayerField((players.children().length/2) + 1, players)
}

function generatePlayerField(number, container){
  let name = "player" + number
  let input = document.createElement("input")
  input.type = "text"
  input.name = name
  input.id = name
  input.setAttribute("placeholder", name)
  input.setAttribute("required", true)
  input.setAttribute("class", "form-control")
  let label = document.createElement("label")
  label.setAttribute("for", name)
  
  container.append(label, input)
}

