

let generate = {}

generate.graphLinear = ({size}) => {
  let graph = [];

  for(x = 0;x<size;x++){
    graph.push(x)
  }

  return graph
}

generate.graphRandom = ({size, variance}) => {
  let graph = [];

  for(x = 0;x<size;x++){
    graph.push(Math.random() * variance-variance/2)
  }

  return graph
}

generate.graphGaussian = ({size, variance}) => {
  let graph = [];

  for(x = 0;x<size;x++){
    graph.push(randn_bm(1, variance))
  }

  return graph
}

generate.graphPrevGaussian = ({size, variance}) => {
  let graph = [];

  let prev = 0

  for(x = 0;x<size;x++){
    prev = randn_bm(prev, variance)
    graph.push(prev)
  }

  return graph
}

generate.graphDeltaGaussian = ({size, variance}) => {
  let graph = [];
  let prev = 0
  let delta = 0

  for(x = 0;x<size;x++){
    let val = randn_bm(delta, 1)
    delta = val-prev
    graph.push(val)
    prev = val
  }

  return graph
}


function randn_bm(mean, variance) {
  var u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  var bm = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

  return (bm)*variance + mean
}