google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(doDraw);

function draw(generate, size, variance){
  //let data = generate();

  var data = new google.visualization.DataTable();
  data.addColumn('number', 'X');
  data.addColumn('number', 'Value');
  //data.addColumn('number', 'Cats');
  let graph = generate({size, variance})
  data.addRows(formatGraph(graph));

  var options = {
    colors: ['#a52714', '#097138'],
    explorer: {
      axis: 'horizontal',
      keepInBounds: true,
      maxZoomIn: 4.0
    },
  };

  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}

function formatGraph(graph){
  let data = []
  for(let i=0;i<graph.length;i++){
    data.push([i, graph[i]])
  }
  return data
}


function doDraw(){
  draw(defaults.graph, 100, 10)

}