var sel = $('<select>').appendTo('#options')
for (const property in generate) {
  sel.append($("<option>").attr('value',property).text(property));
}
sel.val(Object.keys(generate).find(key => generate[key] === defaults.graph));


var size = createOption("size", $('<input>').attr("type", "number"), defaults.size)
var variance = createOption("variance", $('<input>').attr("type", "number"), defaults.variance)




var do_generate = $('<button>').appendTo('#options').text("generate");
do_generate.click(() => {
  draw(generate[sel.val()], size.val(), variance.val())
})

function createInput(name, input, parent, value){
  parent = $('<div>').appendTo(parent)
  label = $('<label>').appendTo(parent)
  label.text(name + ":")
  input.appendTo(parent)
  input.val(value)
  return input
}

function createOption(name, option, defaultValue){
  return createInput(name, option, $("#options"), defaultValue)
}

