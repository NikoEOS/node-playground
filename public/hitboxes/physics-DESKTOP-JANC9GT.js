class Force {
    constructor(spring, friction) {
        this.spring = spring;
        this.friction = friction;
    }
}




function initPhysics(object, mass, frictionMultiplier, elasticity){
    object.acceleration = new Point(0,0);
    object.speed = new Point(0,0);
    object.totalForce = new Point(0,0);
    object.springForce = new Point(0,0);
    object.frictionForce = new Point(0,0);
    object.forceList = [object.springForce, object.frictionForce]
    //console.log(object.springForce.x);
    if(typeof mass !== 'undefined'){
        object.mass = mass;
    }else{
        object.mass = 1000;    
    }
    if(typeof frictionMultiplier !== 'undefined'){
        object.frictionMultiplier = frictionMultiplier;
    }else{
        object.frictionMultiplier = 0;
    }
    if(typeof elasticity !== 'undefined'){
        object.elasticity = elasticity;
    }else{
        object.elasticity = 1;
    }
}

function physicsCalculateAll(object){
    if(object.mouseDown){
        console.log(object.id);
        physicsCalculateTotalForce(object);
        physicsCalculateAcceleration(object);
        physicsCalculateSpeed(object);
    }
      
    physicsCalculatePosition(object);  
}

function physicsCalculatePosition(object){
    object.pos.x += object.speed.x;
    object.pos.y += object.speed.y;
}

function physicsCalculateSpeed(object){
    object.speed.x += object.acceleration.x;
    object.speed.y += object.acceleration.y;
}

function physicsCalculateAcceleration(object){
    if(object.mass == 0){
        object.mass = 0.0001;
    }
    object.acceleration.x = object.totalForce.x/object.mass;
    object.acceleration.y = object.totalForce.y/object.mass;
    object.totalForce.x = 0;
    object.totalForce.y = 0;     
}

function physicsCalculateTotalForce(object){
    object.totalForce = object.springForce;
    //console.log("total force: " + object.totalForce.x + ", " + object.totalForce.y) 
}

function physicsSetSpeed(object, speed){
    object.speed.x = speed.x;
    object.speed.y = speed.y;
}

function physicsPolarToCartesian(amount, angle){
    //console.log("P2C: " + amount + ", " + angle);
    x = Math.cos(angle)*amount;
    y = Math.sin(angle)*amount;
    return new Point(x,y);
}

function physicsAngleOfVector(vector){
    //console.log(vector.x + ", " + vector.y);
    if(vector.x == 0 && vector.y == 0){
        console.log("Cannot calculate angle of vector (0,0)");
        return Math.PI/2;
    }
    else if(Math.abs(vector.x) > Math.abs(vector.y)){
        console.log("");
        if(direction(vector.x) == 1){
            return Math.atan(vector.y/vector.x) + Math.PI;     
        }
        return Math.atan(vector.y/vector.x);    
     
    }else{
        if(direction(vector.y) == 1){
            console.log("y abs bigger y negative");
            return Math.PI/-2 - Math.atan(vector.x/vector.y);
        }else{
            console.log("y abs bigger else");
            return Math.PI/2 - Math.atan(vector.x/vector.y);
        }        
    }    
}

function physicsSpringForceBetween(pos1, pos2, springMultiplier){
    return Point.distance(pos1, pos2) * -springMultiplier;
}

function physicsSetSpringForce(object, position, springMultiplier){
    amount = physicsSpringForceBetween(position, object.pos, springMultiplier);
    vector = new Point(position.x - object.pos.x, position.y - object.pos.y);
    angle = physicsAngleOfVector(vector);
    console.log(angle);
    force = physicsPolarToCartesian(amount, angle);
    //console.log(force.x);
    object.springForce = force;
}

