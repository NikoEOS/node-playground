
//TODO: cleanup

function checkCollisions(hitboxes){
    
}


function onSquareCollision(object){

}

function randomBetween(min, max){ 
    return Math.random() * (+max - +min) + +min; 
}    


function collision(object, hitbox, refreshWindow){
    if(object.mouseDown){
        if (Math.abs(object.pos.x) + object.width/2 >= hitbox.offsetWidth/2){
            object.speed.x = 0;//-1*direction(object.speed.x);
            object.pos.x = (hitbox.offsetWidth/2 - object.width/2) * direction(object.pos.x);   
        }
        if (Math.abs(object.pos.y) + object.height/2 >= hitbox.offsetHeight/2){
            object.speed.y = 0;//-1*direction(object.speed.y);
            object.pos.y = (hitbox.offsetHeight/2 - object.height/2) * direction(object.pos.y); 
        }   
    }else{
        if (Math.abs(object.pos.x) + object.width/2 > hitbox.offsetWidth/2){
            
            object.speed.x = -object.speed.x;

            var color = 'hsl('+randomBetween(0,360)+', 50%, 50%);';
            object.colorDiv.style = 'background-color: ' + color;

            refreshWindow.style.zIndex = 2;
        }
        if (Math.abs(object.pos.y) + object.height/2 > hitbox.offsetHeight/2){
           
            object.speed.y = -object.speed.y;

            var color = 'hsl('+randomBetween(0,360)+', 50%, 50%);';
            object.colorDiv.style = 'background-color: ' + color;

            refreshWindow.style.zIndex = 2;
        }
    }   
}

function isInsideHitbox(pos, object){
    
    if (Math.abs(pos.x) + object.width/2 >= object.wrapper.offsetWidth/2 || Math.abs(pos.y) + object.height/2 >= object.wrapper.offsetHeight/2){
        return true;
    }
    else{
        return false;
    }    
}