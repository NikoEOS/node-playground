
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
  
    static distance(a, b) {
        const dx = a.x - b.x;
        const dy = a.y - b.y;
        return Math.hypot(dx, dy);
    }
}

class Info {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
  
    static sendTotalDistance(d) {
        document.getElementById("distanceTravelled").innerHTML = "" + d;
    }
}

var tickInterval = 16;

setInterval(onTimerTick, 16); // 33 milliseconds = ~ 30 frames per sec

var secondInTicks = 1000/tickInterval;

var myObject = new Object();

myObject

myObject.spring = 10;
myObject.mass = 1000;
myObject.frictionMultiplier = 50;

var gravitation = 4000;

myObject.distanceTravelled = 0;

myObject.xAxisLocked = false;
myObject.yAxisLocked = false;

var pushForceMultiplier = 1;
myObject.xPushForce = 0;

myObject.mouseForce = new Point(0,0);
noClickMode = false;
noClickMouseDistanceRequired = 75;

var mouseForceMultiplier = 10;

myObject.springLength = -1;

var trigonometryMode = true;

myObject.DOM = document.getElementsByClassName("forceObject")[0];
myObject.Wrapper = document.getElementsByClassName("forceWrapper")[0];

myObject.DOM.draggable = false;

updateObject();
updateMaxCoords();

myObject.xSpeed = 0;
myObject.ySpeed = 0;
myObject.xPos = 0;
myObject.yPos = 0;
myObject.pos = new Point(myObject.xPos, myObject.yPos);
var center = new Point(myObject.wrapperCenterX, myObject.wrapperCenterY);

//myObject.pos.x = myObject.xTop;
//myObject.pos.y = myObject.yTop;
changeImagePos(myObject.xTop,myObject.yTop);

function onTimerTick(){
    updateObject();
    debug(myObject.mass);
    //myObject.DOM.innerHTML = Acceleration(xSpringForce()) + "\n" + Acceleration(ySpringForce());
    move();
    setLine();
    changeImagePos(myObject.pos.x, myObject.pos.y);
    Info.sendTotalDistance(myObject.distanceTravelled);
}

function changeImagePos(x, y){
    var xOffset = (myObject.Wrapper.offsetWidth/2)-myObject.DOM.offsetWidth/2;
    var yOffset = (myObject.Wrapper.offsetHeight/2)-myObject.DOM.offsetHeight/2;
    if(true){
        if(!myObject.xAxisLocked){
            myObject.DOM.style.left = xOffset + x + 'px';
        }else{
            myObject.DOM.style.left = xOffset + 'px';
            myObject.pos.x = 0;
            myObject.xSpeed = 0;    
        }
        if(!myObject.yAxisLocked){
            myObject.DOM.style.top = yOffset + y + 'px';
        }else{
            myObject.DOM.style.top = yOffset + 'px';
            myObject.pos.y = 0;
            myObject.ySpeed = 0; 
        }
    }
       
}

function calculateSpeed(){
    console.log(myObject.springLength);
    myObject.distanceTravelled += calculateTotalSpeed(new Point(myObject.xSpeed, myObject.ySpeed));
    /*
    if(Point.distance(myObject.pos, center)<=myObject.springLength || myObject.springLength == -1){
        myObject.xSpeed += Acceleration(xForces());
        myObject.ySpeed += Acceleration(yForces());    
    }else if(Point.distance(myObject.pos, center)>=myObject.springLength){
        myObject.xSpeed -= Acceleration(xForces());
        myObject.ySpeed -= Acceleration(yForces());        
    }else{
        myObject.xSpeed = 0;
        myObject.ySpeed = 0; 
    }*/
    myObject.xSpeed += Acceleration(xForces());
    myObject.ySpeed += Acceleration(yForces()); 

}

function calculateTotalSpeed(speed){
    return Math.hypot(speed.x, speed.y);
}

function move(){
    calculateSpeed();
    myObject.pos.x += myObject.xSpeed;
    myObject.pos.y += myObject.ySpeed;
    
    //console.log("x: " + myObject.pos.x + " y: " + myObject.pos.y);
    //console.log("xSpeed: " + myObject.xSpeed + " ySpeed: " + myObject.ySpeed);   
}

function updateObject(){
    myObject.wrapperCenterX = myObject.Wrapper.offsetLeft + myObject.Wrapper.offsetWidth / 2;
    myObject.wrapperCenterY = myObject.Wrapper.offsetTop + myObject.Wrapper.offsetHeight / 2;
    myObject.objectCenterX = myObject.DOM.offsetLeft + myObject.DOM.offsetWidth / 2;
    myObject.objectCenterY = myObject.DOM.offsetTop + myObject.DOM.offsetHeight / 2;
}

function changeMass(m){
    myObject.mass = m;
    alert("mass: "+myObject.mass);
}

function changeXAxisLock(){
    if(myObject.xAxisLocked){
        myObject.xAxisLocked = false;
    }else{
        myObject.xAxisLocked = true;
    }
    return myObject.xAxisLocked; 
}

function changeYAxisLock(){
    if(myObject.yAxisLocked){
        myObject.yAxisLocked = false;
    }else{
        myObject.yAxisLocked = true;
    }
    return myObject.yAxisLocked; 
}

function changeMouseMode(){
    if(noClickMode){
        noClickMode = false;
    }else{
        noClickMode = true;
    }
    //console.log("mouse mode changed");
}

function changetrigonometryMode(){
    if(trigonometryMode){
        trigonometryMode = false;
        console.log("trigMode: off");
    }else{
        trigonometryMode = true;
        console.log("trigMode: on");
    }
    
}

function xDistanceToCenter(){
    return myObject.objectCenterX - myObject.wrapperCenterX;
}

function yDistanceToCenter(){
    return myObject.objectCenterY - myObject.wrapperCenterY;
}

function xDirection(){
    if (xDistanceToCenter() != 0){
        return xDistanceToCenter()/Math.abs(xDistanceToCenter());    
    }else{
        return 0;
    }
    
}
function yDirection(){
    if (yDistanceToCenter() != 0){
        return yDistanceToCenter()/Math.abs(yDistanceToCenter());
    }else{
        return 0;
    }
    
}

function xSpringForce(){   
    return xDistanceToCenter() * -myObject.spring;
}

function ySpringForce(){
    return yDistanceToCenter() * -myObject.spring; 
}

function springForces(){
    if(Point.distance(myObject.pos, center)<=myObject.springLength || myObject.springLength == -1){
        //console.log("xSpringForce: " + xDistanceToCenter()*-myObject.spring + "ySpringForce: " + yDistanceToCenter()*-myObject.spring)
        return new Point(xDistanceToCenter()*-myObject.spring, yDistanceToCenter()*-myObject.spring)
    }else{
        return new Point(xDistanceToCenter()*-(Math.pow(myObject.spring,3)), yDistanceToCenter()*-(Math.pow(myObject.spring,3)));
    }
}

function FrictionForce(){
    return myObject.frictionMultiplier;
}

function xFriction(){
    return -myObject.xSpeed * FrictionForce();
}

function yFriction(){
    return -myObject.ySpeed * FrictionForce();
}

function Acceleration(force) {
    return force/myObject.mass;
}

function xForces(){
    var total = 0;
    
    if(trigonometryMode){
        total += xSpringForceTrig();
    }else{
        total += springForces().x;  
    }
    //console.log("total forces1:" + total); 
    //console.log("x:" + total);
    total += xFriction();
    total += myObject.xPushForce;
    //console.log("total forces3:" + total);
    total += myObject.mouseForce.x;
    return total;
}

function yForces(){
    var total = 0;
    if(trigonometryMode){
        total += ySpringForceTrig(); 
    }else{
        total += springForces().y;  
    }
    
    //console.log("y:" + total);
    total += gravitation;
    //console.log("total yForces2:" + total);
    total += yFriction();
    //console.log("total yForces3:" + total);
    total += myObject.mouseForce.y;
    return total;
}

function totalForceTrig(){
    var help =  new Point(0,0);
    return Point.distance(help, myObject.pos) * -myObject.spring;
}
function xSpringForceTrig(){
    
    var tan = Math.PI/2;
    if(myObject.pos.x != 0){
        tan = Math.atan(myObject.pos.y/myObject.pos.x);
    }
    //console.log(myObject.pos.x);
    //console.log(myObject.pos.x/myObject.pos.y);
    //console.log(Math.atan(myObject.pos.x/myObject.pos.y));
    return totalForceTrig() * Math.cos(tan) * xDirection();

}

function ySpringForceTrig(){
    var tan = Math.PI/2;
    if(myObject.pos.y != 0){
        tan = Math.atan(myObject.pos.x/myObject.pos.y);
    }
        //console.log("yPos: " + myObject.pos.y);     
        //console.log("yTan: " + Math.atan(myObject.pos.x/myObject.pos.y));
        //console.log("yCos: " + Math.cos(Math.atan(myObject.pos.x/myObject.pos.y)));
        return totalForceTrig() * Math.cos(tan) * yDirection();
    
    
}

function updateMaxCoords(){
    myObject.xTop = -myObject.Wrapper.offsetWidth/2 + myObject.DOM.offsetWidth/2;
    myObject.xBottom = myObject.Wrapper.offsetWidth/2 - myObject.DOM.offsetWidth/2;
    myObject.yTop = -myObject.Wrapper.offsetHeight/2 + myObject.DOM.offsetHeight/2;
    myObject.yBottom = myObject.Wrapper.offsetHeight/2 - myObject.DOM.offsetHeight/2;
}

function updateVariables(mass, springMultiplier, frictionMultiplier, gravity, pushMltpr, mouseMltpr, springLength){
    myObject.mass = mass;
    myObject.spring = springMultiplier;
    myObject.frictionMultiplier = frictionMultiplier;
    gravitation = parseFloat(gravity);
    pushForceMultiplier = pushMltpr;
    mouseForceMultiplier = parseFloat(mouseMltpr);
    myObject.springLength = springLength;
    console.log("mass: " + mass +" spring: "+ springMultiplier+" friction: " + frictionMultiplier+" gravity: " + gravity + " push:" + pushForceMultiplier)
    console.log("mouse: "+ mouseForceMultiplier);
}

function getMass(){
    return myObject.mass;
}

function getSpringMultiplier(){
    return myObject.spring;
}

function getFrictionMultiplier(){
    return myObject.frictionMultiplier;
}

function getGravity(){
    return gravitation;
}

function getPushMultiplier(){
    return pushForceMultiplier;
}

function setXPush(force){
    myObject.xPushForce = force*pushForceMultiplier;
}

function getMouseMultiplier(){
    return mouseForceMultiplier;
}

function getSpringLEngth(){
    return myObject.springLength;
}


var mouseDown=false;
var mouseEventTime = 0;
var mouseEventPlace = new Point(0,0);


function mousePlace(event,x,y) {
    console.log("mouse:" + event.mouseX);
    if (typeof x !== 'undefined') {
        mouseX = x;
        mouseY = y;
    }else{
        mouseX = event.clientX;
        mouseY = event.clientY;
    }
    
    posX = mouseX - myObject.wrapperCenterX;
    posY = mouseY - myObject.wrapperCenterY;
    //console.log("mouseX: " + posX + "mouseY: " + posY);
    var mouseXDistance = myObject.pos.x - posX;
    var mouseYDistance = myObject.pos.y - posY;

    if(mouseDown || (noClickMode && (mouseXDistance < noClickMouseDistanceRequired || mouseYDistance < noClickMouseDistanceRequired))){
        
        myObject.mouseForce.x = (mouseXDistance * -mouseForceMultiplier); // + (myObject.xSpeed * myObject.frictionMultiplier/mouseXDistance);
        myObject.mouseForce.y = mouseYDistance * -mouseForceMultiplier; // + (myObject.ySpeed * myObject.frictionMultiplier/mouseYDistance);

        /*
        changeImagePos(posX, posY);
        myObject.pos.x = posX;
        myObject.pos.y = posY;
        if(true){
            myObject.xSpeed = 0;
            myObject.ySpeed = 0;
        }
        */
    }else{
        myObject.mouseForce.x = 0;
        myObject.mouseForce.y = 0;
    }       
}

myObject.DOM.addEventListener("mousedown", setMouseDown);
document.addEventListener("mouseup", setMouseUp);
document.addEventListener("mousemove", mousePlace);
document.addEventListener("touchstart",setMouseDown);
document.addEventListener("touchend", touchEnd);
document.addEventListener("touchmove", touchMove);

function touchMove(event){
    //alert("touch");
    event.clientX = event.touches[0].clientX;
    event.ClientY = event.touches[0].clientY;
    console.log(event.clientX);
    mousePlace(event,event.touches[0].clientX,event.touches[0].clientY);
}
function touchEnd(){
    mouseDown=false;
    myObject.mouseForce.x = 0;
    myObject.mouseForce.y = 0;

}

function setMouseDown(){
    mouseDown=true;
}

function setMouseUp(){
    mouseDown=false;
}

function scroll(event){
    alert(event);
}

//document.addEventListener("scroll", scroll);

function setLine(){
    var line = document.getElementById("l1");
    line.x1.baseVal.value = myObject.wrapperCenterX;
    line.y1.baseVal.value = myObject.wrapperCenterY;
    line.x2.baseVal.value = myObject.objectCenterX;
    line.y2.baseVal.value = myObject.objectCenterY;
}

function debug(input){
    document.getElementsByClassName("debug")[0].innerHTML = input;    
}

function realtiveAcceleration(x,y){


}

