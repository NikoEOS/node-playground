

var pos = new Point(0,0);

var activeObject;

function getActiveObject(){
    return activeObject;
}

function initMouse(){
    /*
        sets needed event listeners for mouse and touch + gets object
                                                                                  
        it needs to be a function to make sure everything is available for init   
    */
    activeObject = getSomeObject();
    document.addEventListener("mousemove", placeMyObject);
    document.addEventListener("mouseup", setMouseUp);
    document.addEventListener("touchend", setMouseUp);

}

function setUpMouse(object){
    object.element.addEventListener("mousedown", setMouseDown);
    object.element.addEventListener("touchmove", touchMove);
    object.element.addEventListener("ondragstart", (e) => {
        e.preventDefault();
        return false;
    });
}

function placeObject(event,x,y) {
    /*
        sets mouses pos to be used in placing objects

        uses var mouseDown as flag whether to take action or not

        uses x and y input as coordinates instead if defined. Useful for handling touch events.
    */
    if (typeof x !== 'undefined') {
        mouseX = x;
        mouseY = y;
    }else{
        mouseX = event.clientX;
        mouseY = event.clientY;
    }

    if(activeObject.mouseDown){
        center = getobjectCenterPosition(activeObject);
        pos.x = mouseX - center.x
        pos.y = mouseY - center.y
    }      
}

function placeMyObject(event,x,y){
    placeObject(event,x,y);
}

function touchMove(e){
    activeObject = setMainMouseDownById(e.target.id.replace('Color',''));
    placeObject(e,e.touches[0].clientX,e.touches[0].clientY);
}

function touchEnd(){
    setMouseUp();
}

function setMouseDown(e){
    /*
        set mouseDown attribute of the object that triggered the event to true and sets that element as active
    */
    activeObject = setMainMouseDownById(e.target.id.replace('Color',''));
}

function setMouseUp(e){
    setMainMouseUp();
}

function scroll(event){
    alert(event);
}

function updateMouse(object){
    if (object.mouseDown){
       physicsSetSpeed(object, new Point((pos.x-object.pos.x)*0.05, (pos.y-object.pos.y)*0.05)); 
    }     
}

function mouseAction(){

}