


class Info {
    /*
        class to store outgoing info

        very work in progress
    */
  
    static sendTotalDistance(d) {
        document.getElementById("distanceTravelled").innerHTML = "" + d;
    }
}




var refreshWindow = document.getElementById("wrapperRefresh");

var objects = [];

function addObject(id){
    var object = createObject(id);
    initPhysics(object);
    objects.push(object);
    setUpMouse(object);
    return object;
}

function generateId(){
    if(objects.length != undefined){
        return 'c' + objects.length;
    }   
}

addObject(generateId());
addObject(generateId());




//var center = getobjectCenterPosition(c1);

makeElementStatic(document.getElementById('body'));

initMouse(objects);


let stressTest = new StressTest();
var balls = objects.length;

function onTick(){
    /*
        Main loop of tick engine
    */
    for(var i=0; i < objects.length; i++){
        updateMouse(objects[i]);
        physicsCalculateAll(objects[i]);
        updateElementPosition(objects[i]);
        collision(objects[i], objects[i].wrapper, refreshWindow);
    }
    /*  Stress test */
    if(stressTest.active){
        stressTest.increment();
    }
    
    
    //console.log(getActiveObject());
    refreshWindow.style.zIndex = -2;  
}

function getSomeObject(){
    return objects[0];
}

function debug(input){
    /*
        Prints input to if existing debud container
    */
    document.getElementsByClassName("debug")[0].innerHTML = input;    
}

function setMainMouseUp(){
    for (var i=0; i < objects.length; i++){
            objects[i].mouseDown = false;
    }
}

function setMainMouseDownById(id){
    var object;
    for (var i=0; i < objects.length; i++){
        if(objects[i].id==id){
            objects[i].mouseDown = true;
            object = objects[i];    
        }else{
            objects[i].mouseDown = false;
        }
    }
    return object;
}

function StressTest(){
    this.n = 0;
    this.a = -Math.PI;
    this.active = false;
    this.increment = function (){
        if(this.n==10){
            object = addObject(generateId());
            physicsSetSpeed(object, physicsPolarToCartesian(10,this.a))
            this.n=0;
            this.a += Math.PI/45;
            if(this.a == Math.PI){
                this.a = 0;
            }
            if(fps<50){
                stopStressTest(objects.length);
            }
        }
        this.n++;
    }
}


function startStressTest(){
    stressTest.active = true;  
}

function stopStressTest(result){
    stressTest.active = false;
    document.getElementById("stressTestResult").innerHTML = "Balls: " + result; 
}

Date.prototype.timeNow = function () {
    return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}




/*
    Run
*/

function render() {
    onTick();
    requestAnimationFrame(render);
}
  
render();