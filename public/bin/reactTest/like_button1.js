

const schema = {
  type: "object",
  properties: {
    foo: {
      type: "object",
      properties: {
        bar: {type: "string"}
      }
    },
    baz: {
      type: "array",
      items: {
        type: "object",
        properties: {
          description: {
            "type": "string"
          }
        }
      }
    }
  }
}

const uischema = {
  foo: {
    bar: {
      "ui:widget": "textarea"
    }
  },
  baz: {
    // note the "items" for an array
    items: {
      description: {
        "ui:widget": "textarea"
      }
    }
  }
}

'use strict';

var form = document.createElement;
form.schema = schema;
form.uiSchema = uischema;

const e = React.createElement;



const domContainer = document.querySelector('#like_button_container');
ReactDOM.render(e(
  'form',
  { id: "moi", onClick: console.log(""), schema: schema, uischema: uischema },
  ''
), domContainer);


