




class Person extends React.Component {
  constructor(props) {
    super(props);
    this.state = {name: "Niko"};
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form action="">
        <input type="text" value={this.state.name}></input>
        <input type="datetime-local"></input>
      </form>
    );
  }
}

ReactDOM.render(<Person />, document.getElementById('root'));
