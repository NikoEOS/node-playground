
function initClient(){
    return stitch.Stitch.initializeDefaultAppClient('firsttest-apioy');
}

function getDb(client) {
    return client.getServiceClient(stitch.RemoteMongoClient.factory, 'mongodb-atlas').db('Register');
}

function findPersons(client, db){
    client.auth.loginWithCredential(new stitch.AnonymousCredential()).then(() =>
        db.collection('Person').find({}, { limit: 100}).asArray()
    ).then(docs => {
        console.log(docs);
    }).catch(err => {
        console.error(err)
    });
}


function updatePerson(client, db, person){
    client.auth.loginWithCredential(new stitch.AnonymousCredential()).then(user =>
        db.collection('Person').updateOne({owner_id: client.auth.user.id}, {$set:person}, {upsert:false})
    ).catch(err => {
        console.error(err)
    });
}


function testUpdatePerson(client, db, person){
    client.auth.loginWithCredential(new stitch.AnonymousCredential()).then(user =>
        db.collection('Person').updateOne({owner_id: client.auth.user.id}, {$set:person}, {upsert:true})
      ).then(() =>
        db.collection('Person').find({owner_id: client.auth.user.id}, { limit: 100}).asArray()
      ).then(docs => {
          console.log("Found docs", docs)
          console.log("[MongoDB Stitch] Connected to Stitch")
      }).catch(err => {
        console.error(err)
      });
}