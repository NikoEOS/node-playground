initArea = () => {
  let area = new Object();
  area.je = $("#cubeArea");
  area.maxSpeed = -1;

  console.log(area.je.position());

  area.pos = area.je.position();
  area.size = { left: area.je.width(), top: area.je.height() };

  area.je.mousemove(e => {
    //console.log(e);
    //console.log(area.activeCube);
    let activeCube = area.activeCube;
    if (activeCube) {
      let speed = {};
      speed.left = Math.floor(0.1 * (e.clientX - activeCube.pos.left));
      speed.top = Math.floor(0.1 * (e.clientY - activeCube.pos.top));
      activeCube.speed = speed;
    }
  });

  $(document).mouseup(e => {
    area.activeCube = null;
  });

  return area;
};

createCube = (id, area) => {
  const cube = new Object();
  cube.id = id;
  initCube(cube, area);

  cube.move = (x, y) => {
    cube.pos.left = x;
    cube.pos.top = y;
    cube.je.offset(cube.pos);
  };

  cube.addOffset = toAdd => {
    cube.move(cube.pos.left + toAdd.left, cube.pos.top + toAdd.top);
  };

  cube.checkBoundaries = () => {
    return checkBoundaries(cube, area);
  };

  cube.checkSpeedLimit = () => {
    return checkSpeedLimit(cube, area);
  };

  cube.onTick = mouse => {
    const didHit = cube.checkBoundaries();
    cube.checkSpeedLimit();
    cube.addOffset(cube.speed);
    return didHit;
  };

  console.log(cube.je);
  console.log($(document));
  return cube;
};

initCube = (cube, area) => {
  cube.pos = { top: area.size.top / 2, left: area.size.left / 2 };
  cube.speed = { top: 0, left: 0 };

  cube.e = document.createElement("div");
  $("#cubeArea").append(cube.e);
  cube.je = $(cube.e);
  cube.je.addClass("cube");
  cube.e.setAttribute("id", cube.id);
  cube.je.offset(cube.pos);

  cube.size = { left: cube.je.width(), top: cube.je.height() };

  cube.je.mousedown(e => {
    console.log(cube.id);
    console.log($(document));
    area.activeCube = cube;
  });

  return cube;
};

checkBoundaries = (cube, area) => {
  if (cube.pos.left < area.pos.left) {
    cube.speed.left = Math.abs(cube.speed.left);
    return true;
  } else if (cube.pos.left + cube.size.left > area.pos.left + area.size.left) {
    cube.speed.left = Math.abs(cube.speed.left) * -1;
    return true;
  }

  if (cube.pos.top < area.pos.top) {
    cube.speed.top = Math.abs(cube.speed.top);
    return true;
  } else if (cube.pos.top + cube.size.top > area.pos.top + area.size.top) {
    cube.speed.top = Math.abs(cube.speed.top) * -1;
    return true;
  }
  return false;
};

checkSpeedLimit = (cube, area) => {
  if (area.maxSpeed > 0) {
    let speed = cube.speed;
    if (Math.abs(speed.left) > area.maxSpeed && speed.left > 0) {
      speed.left = (Math.abs(speed.left) / speed.left) * area.maxSpeed;
    }
    if (Math.abs(speed.top) > area.maxSpeed && speed.top > 0) {
      speed.top = (Math.abs(speed.top) / speed.top) * area.maxSpeed;
    }
    cube.speed = speed;
  }
};
