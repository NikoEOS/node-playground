const game = new Object();

game.area = initArea();
game.area.maxSpeed = 1;
game.cubes = new Array();
game.money = 10;

game.cubePrice = variables.cubeInitialPrice;
game.speedPrice = variables.speedInitialPrice;

game.onHit = () => {
  game.money += variables.moneyOnHit;
  console.log(game.money);
};

game.buyCube = () => {
  if (game.money >= game.cubePrice || game.cubes.length <= 0) {
    game.cubes.push(createCube(game.cubes.length, game.area));
    game.money -= game.cubePrice;
  }
};

game.buySpeed = () => {
  if (game.money >= game.speedPrice) {
    game.area.maxSpeed += 1;
    game.money -= game.speedPrice;
    game.speedPrice += 1;
  }
};

game.buyCube();
game.cubes[0].move(200, 200);

const onTick = () => {
  game.cubes.forEach(element => {
    if (element.onTick()) {
      game.onHit();
    }
  });
  $("#score").text(game.money);
};

setInterval(onTick, 7);
