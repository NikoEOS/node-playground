function qs(search_for) {
    let searchParams = new URLSearchParams(window.location.search)
    return searchParams.get("name");
}

function setBox() {			
    var width = document.getElementById('box').offsetWidth;
    var height = document.getElementById('box').offsetHeight;
    if (height>width){
        document.getElementById('box').setAttribute("style","height:"+width);
    }else if (width>height) {
        document.getElementById('box').setAttribute("style","width:"+height);
    }				
}

giftOpen = () => {
    $(".gift").addClass("open");
}

$(document).ready(()=>{
    let searchParams = new URLSearchParams(window.location.search)
    $("#to").text(searchParams.get("to"));
    $("#from").text(searchParams.get("from"));
})