var data = [];



function startWork(form){
  form.startTime.value = new Date();
  $(form).children(".start").hide();
  $(form).children(".stop").show();
  save();
}

function stopWork(form){
  var seconds = (new Date().getTime() - new Date(form.startTime.value).getTime()) / 1000;
  form.time.value = secondsToString(stringToSeconds(form.time.value) + seconds);
  form.startTime.value = "";
  console.log(form);
  $(form).children(".stop").hide();
  $(form).children(".start").show();
  save();
}

function printData(){
  console.log(data);
}

function printCookie(){
  console.log(document.cookie);
}

function read(){
  data = JSON.parse(document.cookie).data;
  for(var i=0;i<data.length; i++){
    newFormWithData(data[i]);
  }
}

function save(){
  var forms = $("#main").children("form");
  var data=[];
  forms.each(function() {
    data.push($(this).serializeArray());
  });
  document.cookie = JSON.stringify({"data":data});
}

function secondsToString(seconds){
  var hours = Math.floor(seconds/3600);
  var hoursRemain = seconds%3600;
  var minutes = Math.floor(hoursRemain/60);
  var remain = Math.floor(hoursRemain%60);

  if(hours < 10){
    hours = "0" + hours;
  }
  if(minutes < 10){
    minutes = "0" + minutes;
  }
  if(remain < 10){
    remain = "0" + remain;
  }

  var string = hours + ":" + minutes +":"+remain;
  return string;
}

function stringToSeconds(string){
  var arr = string.split(":");
  var seconds = parseInt(arr[0], 10) * 3600;
  seconds += parseInt(arr[1], 10) * 60;
  seconds += parseInt(arr[2], 10);
  console.log(seconds);
  return seconds;
}


function newForm(){
  var form = $("#exampleForm").children().clone()
  $("#main").append(form);
  console.log(form);
  data.push(form.serializeArray());
  form.children().change(function() {
    save();
  });
}



function newFormWithData(data){
  console.log(data);
  var form = $("#exampleForm").children().clone()
  $("#main").append(form);
  form.children().change(function() {
    save();
  });
  var inputs = form.children("input");
  for(var i=0;i<inputs.length;i++){
    inputs[i].value = data[i].value
  }
  if(data[2].value == ""){
    $(form).children(".stop").hide();
    $(form).children(".start").show();
  }else{
    $(form).children(".stop").show();
    $(form).children(".start").hide(); 
  }

}

