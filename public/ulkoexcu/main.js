


var selectedKey = config.days[new Date().getDay()];


$(document).ready(function (){
    addDays(Object.keys(config.schedule)); 
    selectDate(selectedKey);  
});


function addDays(days){
    
    for(day in days){
        let element = $(document.createElement('li'));
        element.addClass("day");
        element.append(days[day]); 
        element.attr("date",config.schedule[days[day]].date);
        element.attr("key",days[day]);
        element.click(function(){
            selectDate($(this).attr("key"));
        });
        $( ".days" ).append(element); 
    }   
}

function selectDate(key){

    selectedKey = key;
    $(".days li").removeClass("selected");
    let el = $("[key='"+selectedKey+"']");
    el.addClass("selected");
    renderDayData(el.attr("key"));
}

function renderDayData(key){
    let main = $(".mainContent");
    let data = config.schedule[key];

    emptyAll(main);

    let date = data.date;
    let stay = data.destination;

    let month = 10;
    let year = date.getFullYear()


    main.find("#day").append(config.daysFull[date.getDay()]);
    main.find("#date").append(date.getDate() + "." + month + "." + date.getFullYear());
    main.find("#location").append("@"+stay.city);
    
    main.find("#address").append(stay.address);

    if(stay.link != ""){
        main.find("#stay").append("<a href='"+stay.link+"'>"+stay.hotel+"</a>");
    }else{
        main.find("#stay").append(stay.hotel);
    }

    if(data.activities != undefined && data.activities.length > 0){
        data.activities.forEach((element, index) => {
            renderActivity(element, index);    
        });
    }else{
        $("#activities").append("Aika tyhjää");
    }
    

    

}

function emptyAll(main){
    main.find("div[id]").each(function( index, value ) {  
        $(value).empty();
    });
}

function renderActivity(activity, index){
    let temp = $(".activityTemplate:not([id])").clone();

    temp.attr("id","a"+index);

    temp.find("#icon").append(activity.icon);
    temp.find("#time").append(activity.time);
    temp.find("#text").append(activity.text);

    if(activity.location != "" && activity.location != undefined){
        temp.find("#location").append("<i class='fas fa-map-marker-alt'></i>" + activity.location);
    }
    

    $("#activities").append(temp);
}