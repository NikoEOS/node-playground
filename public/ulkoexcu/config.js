var utc = "0020"



var destinations = {
    Prague: {
        city: "Praha",
        hotel: "Stary Pivovar",
        address: "Plzeňská 229, 150 00 Praha 5, Tšekki",
        link: "https://www.google.fi/maps/place/Hotel+Stary+Pivovar/@50.0668498,14.3279763,17z/data=!3m1!4b1!4m5!3m4!1s0x470b945674db545b:0x64d9e75df58bf47c!8m2!3d50.0668498!4d14.330165"
    },
    Berlin: {
        city: "Berliini",
        hotel: "EASY Lodges Berlin",
        address: "Columbiadamm 160, 10965 Berlin, Saksa",
        link: "https://www.google.fi/maps/place/Easy+lodges+Berlin/@52.4797,13.4141813,17z/data=!3m1!4b1!4m8!3m7!1s0x47a84fc08f5ea785:0x2ecd7693d69c25b4!5m2!4m1!1i2!8m2!3d52.4797!4d13.41637"
    },
    Transfer: {
        city: "Praha - Dresden - Berliini",
        hotel: "EASY Lodges Berlin",
        address: "Columbiadamm 160, 10965 Berlin, Saksa",
        link: "https://www.google.fi/maps/place/Easy+lodges+Berlin/@52.4797,13.4141813,17z/data=!3m1!4b1!4m8!3m7!1s0x47a84fc08f5ea785:0x2ecd7693d69c25b4!5m2!4m1!1i2!8m2!3d52.4797!4d13.41637"
    },
    Home: {
        city: "Mahdollinen krapula",
        hotel: "Niinno",
        address: "???",
        link: ""
    }
}

var config = {
    schedule: {
        Ma:{
            date: new Date(2019, 9, 7),
            activities:[
                {
                    time: "00:00 ➜",
                    text: "Prague & chill",
                    icon: '<i class="fas fa-globe-europe"></i>',
                    color: "",
                    isPaid: false,    
                }
            ],
            destination: destinations.Prague      
        },
        Ti:{
            date: new Date(2019, 9, 8),
            activities:[
                {
                    time: "9:00???",
                    text: "Bussi lähtee kohteeseen ???",
                    icon: '<i class="fas fa-question"></i>',
                    color: "",
                    isPaid: true,
                    location: "???"    
                }
            ],
            destination: destinations.Prague   
        },
        Ke:{
            date: new Date(2019, 9, 9),
            activities:[
                {
                    time: "18:00",
                    text: "Panimokierros (sen varanneille)",
                    icon: '<i class="fas fa-beer"></i>',
                    color: "",
                    isPaid: true,    
                }
            ],
            destination: destinations.Prague  
        },
        To:{
            date: new Date(2019, 9, 10),
            activities:[
                {
                    time: "???",
                    text: "Juna lähtee prahasta",
                    icon: '<i class="fas fa-train"></i>',
                    color: "",
                    isPaid: false,
                    location: "Prague Main Station"    
                },
                {
                    time: "12:18",
                    text: "Juna saapuu Dresdeniin",
                    icon: '<i class="fas fa-train"></i>',
                    color: "",
                    isPaid: false,
                    location: "Dresden Central Station"    
                }
            ],
            destination: destinations.Transfer  
        },
        Pe:{
            date: new Date(2019, 9, 11),
            activities:[

            ],
            destination: destinations.Berlin 
        },
        La:{
            date: new Date(2019, 9, 12),
            activities:[
                {
                    time: "Joskus",
                    text: "Mennään kotiin",
                    icon: '<i class="fas fa-plane"></i>',
                    color: "",
                    isPaid: false,   
                }
            ],
            destination: destinations.Home
        }
    },
    days: [
        "Su", "Ma", "Ti", "Ke", "To", "Pe", "La"
    ],
    daysFull: [
        "Sunnuntai", "Maanantai", "Tiistai", "Keskiviikko", "Torstai", "Perjaintai", "Lauantai"
    ]
}


 