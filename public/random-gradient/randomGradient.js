let random = new Object;

random.rgb = (isOpacity) => {
    rgb = new Object;
    rgb.red = Math.floor((Math.random() * 255) + 1);
    rgb.green = Math.floor((Math.random() * 255) + 1);
    rgb.blue = Math.floor((Math.random() * 255) + 1);
    if(isOpacity == undefined || isOpacity == false){
        rgb.opacity = 1;
    }else{
        rgb.opacity = Math.random(); 
    }
    rgb.stringify = "rgba(" + rgb.red + "," + rgb.green + "," + rgb.blue + "," + rgb.opacity + ")";

    return rgb;
}

random.linearGradient = (n=1, hasPercentage=false, isTilted=false) => {
    linearGradient = new Object;
    
    if(n==-1){
        n = Math.floor((Math.random() * 255) + 1);    
    }else if(n<1){
        n = 1;
    }

    if(isTilted){
        linearGradient.tilt = Math.floor((Math.random() * 360) + 1); 
    }else{
        linearGradient.tilt = 0;
    }
    
    let step = (lastPercentage) => {
        let step = new Object;
        if(lastPercentage != undefined){
            step.percentage = Math.floor((Math.random() * (100-lastPercentage)) + lastPercentage);
            step.value = random.rgb().stringify + " " + step.percentage + "%";
            return step;   
        }else{
            step.value = random.rgb().stringify
        }
        return step; 
    }
    
    let currentStep = step();
    if(hasPercentage){
        currentStep = step(0);
    }
    
    linearGradient.steps = [currentStep.value];

    for(var i=0; i<n; i++){
        currentStep = step(currentStep.percentage);
        linearGradient.steps.push(currentStep.value);
    }

    let stringify = (steps, tilt) => {
        let string = "linear-gradient(" + tilt + "deg";
        steps.forEach(s => {
            string += ", " + s;    
        });
        return string + ")";
    }

    linearGradient.stringify = stringify(linearGradient.steps, linearGradient.tilt);
    return linearGradient;
}

//
//console.log($(document.body));



