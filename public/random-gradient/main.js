$(document).ready(function(){
    $("body").css("background",random.linearGradient(-1, true, true).stringify);
});

$("form").each(function(i, obj) {
    $(obj).submit(function(e) {
        e.preventDefault();
    });
});

function onSelect(value){
    $('.options').each(function(i, obj) {
    if($(obj).hasClass(value)){
        $(obj).show();
    }else{
        $(obj).hide();
    }   
    },value); 
}

function generateColor(form){
    console.log(form);
    let opacity = $(form).find("#opacity").is(':checked');
    console.log(opacity);

    $("body").css("background",random.rgb(opacity).stringify);
    return false;
}

function generateGradient(form){
    console.log(form);
    let tilt = $(form).find("#tilt").is(':checked');
    let hasPercentage = !$(form).find("#distribute").is(':checked');
    let amount = $(form).find("#amount").val();
    console.log(opacity);

    $("body").css("background",random.linearGradient(amount, hasPercentage, tilt).stringify);
    return false;
}