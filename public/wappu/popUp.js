function showPopUp(content, link){
    //document.getElementsByClassName("popupBg")[0].style.display = "flex";
    //document.getElementsByClassName("popupBg")[0].style.opacity = "1";
    document.getElementsByClassName("mainBg")[0].className += " blur";
    document.getElementsByClassName("popupContent")[0].innerHTML = "";
    var p = document.createElement("P")
    p.innerHTML = content;

    document.getElementsByClassName("popupContent")[0].appendChild(p);
    getBeverage();
    //<a href="url">link text</a>
 
    
    
    
     
    window.setTimeout( function() {
    
        document.getElementsByClassName("popupBg")[0].className += " visible";
        
    }, 50);
}

function hidePopUp(){
    //document.getElementsByClassName("popupBg")[0].style.display = "none";

    document.getElementsByClassName("popupBg")[0].className = document.getElementsByClassName("popupBg")[0].className.replace(" visible", "");
    document.getElementsByClassName("mainBg")[0].className = document.getElementsByClassName("mainBg")[0].className.replace(" blur", "");
}

function postLink(link){
    if(link != undefined){
        var div = document.getElementsByClassName("popupLink")[0];
        div.innerHTML = "";
        var p = document.createElement("P");
        var a = document.createElement("A");
        p.innerHTML = "Tähän tilanteeseen sopisi esimerkiksi: ";
        a.innerHTML = "" + link;
        a.href = link;
        div.appendChild(p); 
        div.appendChild(a); 
    }
}