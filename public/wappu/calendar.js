





function createCalendar(days){
    var calendar = findCalendarElement();
    for(var i=0;i<days;i++){
        calendar.appendChild(createCalendarElement(i));
    }
}

function findCalendarElement(){
    var options = document.getElementsByClassName("calendar");
    for(var i=0;i<options.length;i++){
        if(options[i].className.replace("calendar","") == ""){
            return options[i];
        }
    }
    alert("error: calendar element not found");
}

function createCalendarElement(index){
    var subElement = document.createElement("DIV");
    subElement.className = "calendarElementWrapper";
    subElement.id = index + 1;
    subElement.addEventListener('click', function(){click(this);}, false);
    subElement.appendChild(createCalendarElementInner(index));
    return subElement;
}


function createCalendarElementInner(index){
    var content = document.createElement("DIV");
    content.className = "calendarElementContent";



    var subElement = document.createElement("DIV");
    subElement.className = "calendarElementInner";
    subElement.innerHTML = index+1;
    //font1 = (size/33);
    //subElement.style.fontSize = font1 + "px";
    content.appendChild(subElement)

    return content;    
}


function showCalendarElementInfo(elem, info){
    elem.childNodes[0].childNodes[0].innerHTML = info; 
}

function findCalendarElementByDate(date){
    var elems = document.getElementsByClassName("calendarElementWrapper");
    for(var i = 0;i<elems.length;i++){
        if(elems[i].id == date){
            //console.log(event.text);
            return elems[i];
        }
    }
}