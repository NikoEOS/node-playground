
<?php
	//echo json_encode(42);
    $date = "/" . $_GET["month"] . "/" . $_GET["day"];


    $curl = curl_init();
    $url = 'http://history.muffinlabs.com/date';
    $url .= $date;

    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_VERBOSE => true
    ]);
    $result = curl_exec($curl);
    curl_close($curl);
    if (!$result) {
        die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
    } 
    echo json_encode($result);
    
	
?>