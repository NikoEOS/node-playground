var badWords = ["assault","die","death","injury","injured","kill","killed","executed","flood","tsunami",
"earthquake","explosion","bomb","bombing","terror","disaster","war","leak","brutal","murder","convicted","charged",
"struck","deadly", "shooting", "shot", "shots", "fatalities", "fatality", "attack", "massacre", "massacred"];

var months = ["January", "February", "March", "April", "May", "June", "July", "August",
"September", "October", "November", "December"]


var rows = 0;
var cols = 0;
var n = 30;


console.log(document.cookie);

if(getCookie("filterOn") == ""){
    document.cookie = 'filterOn=true';
}

setFilter(getCookie("filterOn") == 'true');

console.log("rows: " + rows + ", cols: " + cols);
var currMonth = months[new Date().getMonth()];

var today = new Date().getDate();


var calendar1 = new Object;
//calendar1.onchange = function(){alert("hep");}
calendar1.day = 1;

var days = 30;

setColumns(days);

createCalendar(days);
adjustBackGround();

//getHistoryNew(new Date().getDate(), new Date().getMonth()+1);

document.getElementsByClassName("popupBg")[0].addEventListener('click', function(){hidePopUp();}, false);

/*
let url = "https://history.muffinlabs.com/date";

function logResults(json){
    console.log(json);
  }
  
$.ajax({
    url: "https://history.muffinlabs.com/date",
    dataType: "jsonp",
    jsonpCallback: "logResults"
});
*/

 

function click(elem){
    if(today >= elem.id){
        getHistory(elem.id, new Date().getMonth() + 1);
    }else if(elem.className.includes("active")){
        showPopUp(elem.childNodes[0].innerHTML);    
    }else{
        alert("eipäs keulita siellä");
    }
}

function jsonCallback(json){
    let date = json.date.split(" ")[1];
    elem = findCalendarElementByDate(date);
    let event = handleData(json);
    afterLoad(elem, event);
}

function getHistory(day, month){
    loading();
    let url = "https://history.muffinlabs.com/date/" + month + "/" + day;
    console.log(url);
    $.ajax({
        url: "https://history.muffinlabs.com/date/" + month + "/" + day,
        type: 'GET',
        success: function(data) {
            jsonCallback(data);
        },
        error: function (data) {
            fallBack(findCalendarElementByDate(day));
        }
    });
}




function getHistoryOld(day, month, elem, callback) {
    loading();
    var oReq = new XMLHttpRequest(); //New request object
    oReq.responseType = 'json';
    oReq.onload = function() {
        //console.log(this.responseText);
        //var json = JSON.parse(this.responseText);
        //console.log(JSON.parse(this.response));
        try{
            var event = handleData(JSON.parse(this.response), elem);
            notLoading();
            callback(elem, event);
        }catch(e){
            console.log(e);
            fallBack(elem, callback);
        }
        
        
    };
    oReq.open("get", "/history.php", true);
    //                               ^ Don't block the rest of the execution.
    //                                 Don't wait until the request finishes to 
    //                                 continue.
    oReq.send();
}



function handleData(json, elem){
    
    console.log(json);
    var day = json.date.split(" ")[1];
    var events = json.data.Events;
    events = filterEvents(events);
    var event = events[Math.floor(Math.random() * events.length)];

    return event;
}


function afterLoad(elem, event){
    notLoading();
    showCalendarElementInfo(elem, event.text);
    elem.className = elem.className + " active";
    //alert(elem.childNodes[0].innerHTML);
    showPopUp(event.text);
    var el = elem;
    var style = window.getComputedStyle(el, null).getPropertyValue('font-size');
    var fontSize = parseFloat(style); 
    elem.childNodes[0].childNodes[0].style.fontSize = "calc(0.5vw + 5px)";
}

function fallBack(elem){
    $.getJSON("test.json", function(json) {
        // this will show the info it in firebug console
        //console.log(json);
        var event = handleData(json,elem);
        console.log(event);
        event.text += "----------this is a sample text------------ error occured";
        notLoading();
        afterLoad(elem, event);
    });
}







function loading(){
    document.getElementsByClassName("loadingWrapper")[0].style.opacity = "1";
    //document.getElementsByClassName("loading")[0].style.display = "flex";
}

function notLoading(){
    document.getElementsByClassName("loadingWrapper")[0].style.opacity = "0";
    //document.getElementsByClassName("loading")[0].style.display = "none";
}

function toggleSideBar(){
    document.querySelector(".sideBarContainer").classList.toggle('active');
    document.querySelector(".burger").classList.toggle('active');
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }