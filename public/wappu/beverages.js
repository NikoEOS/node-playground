
let searchUrl = 'https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=List_of_alcoholic_drinks';
let contentUrl = 'https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles=List_of_alcoholic_drinks';

function drinkJsonCallback(json){
    notLoading();
    //for(var i=0;i<15;i++){
        let page = json.query.pages;
        let pageId = Object.keys(json.query.pages)[0];
        //console.log(pageId);
        let content = page[pageId].revisions[0]['*'];
        let arr =content.split("[[");
        arr.splice(0,10);
        let rand = arr[Math.floor(Math.random() * arr.length)];
        //console.log(rand);
        let string = rand.substr(0, rand.indexOf(']]'));
        if(string.includes("|")){
            console.log(string);
            string = string.substr(0, string.indexOf('|'));
            console.log(string);  
        }
        
        //console.log(string);
        string = string.replace(/\s+/g, '_');
        let link = "https://en.wikipedia.org/wiki/" + string;
        console.log(link);
    //}
    postLink(link);
}

function getBeverage(){
    loading();
    let url = contentUrl;
    console.log(url);
    $.ajax({
        url: url,
        dataType: "jsonp",
        jsonpCallback: "drinkJsonCallback",
        error: function(xhr, status, error){
            console.log("Error!" + xhr.status);
            fallBack(findCalendarElementByDate(day));
        }
    });   
}