function adjustBackGround(){
    var elements = document.getElementsByClassName("main")[0].childNodes;
    var heightOfElements = 0;
    for(var i = 0; i<elements.length; i++){
        if(elements[i].className != undefined){
            if(!(elements[i].className.includes("mainBg")) && !(elements[i]).style.display.includes("none")) {
                heightOfElements += elements[i].offsetHeight;
            }
        }
    }
    var bg = document.getElementsByClassName("mainBg")[0];
    bg.style.height = bg.offsetHeight - heightOfElements - 10 + "px";
}

function setColumns(n){
    var calendar = findCalendarElement();
    if(document.body.offsetWidth < document.body.offsetHeight){
        cols = Math.ceil(Math.sqrt(n));
        rows = Math.floor(Math.sqrt(n));
    }else if(document.body.offsetWidth > document.body.offsetHeight){
        cols = Math.floor(Math.sqrt(n));
        rows = Math.ceil(Math.sqrt(n));
        
    }
    
    if(n%(rows*cols) != 0){
        rows += 1;        
    }
    
    console.log("rows: " + rows + ", cols: " + cols);
    //grid-template-columns: repeat(auto-fill, minmax(8rem, 1fr));
    //                  rows                cols
    //grid-template: repeat(3, 1fr) / repeat(4, 1fr);
    console.log(calendar.style);
    //calendar.style.gridTemplateColumns = "repeat(" + cols + ", minmax(0,1fr))";
    calendar.style.gridTemplate = "repeat("+rows+", minmax(0,1fr)) / repeat("+cols+", minmax(0,1fr))";
}