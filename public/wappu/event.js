var filterOn = true;



function containsAny(str, substrings) {
    for (var i = 0; i != substrings.length; i++) {
       var substring = substrings[i];
       if (str.indexOf(substring) != - 1) {
         return true;
       }
    }
    return false; 
}

function filterEvents(events){
    if(filterOn){
        var filtered = [];
        for(var i=0;i<events.length;i++){
            if(events[i].text != undefined && events[i].text != null){
                if(!containsAny(events[i].text.toLowerCase(), badWords)){
                    filtered.push(events[i]);
                } 
            }   
        }
        //console.log(filtered);
        return filtered;
    }else{
        return events;
    }

}

function toggleOffensiveContent(){
    filterOn = !filterOn;
    setFilter(filterOn)
    if(filterOn){
        
    }else{
        
    }
    console.log($(".offensiveToggleDisplay").find(".off")[0]);
    //$(".offensiveToggleDisplay").find(".off")[0].classList.toggle('active');
    //$(".offensiveToggleDisplay").find(".on")[0].classList.toggle('active');
}

function setFilter(value){
    document.cookie = "filterOn="+value;
    console.log("cookie set: " + document.cookie);
    filterOn = value;
    evaluateFilter();
}

function evaluateFilter(){
    console.log(filterOn);
    if(filterOn){
        $(".offensiveToggleDisplay").find(".off")[0].classList.add('active');
        $(".offensiveToggleDisplay").find(".on")[0].classList.remove('active');
        console.log("off");
    }else if(!filterOn){
        $(".offensiveToggleDisplay").find(".off")[0].classList.remove('active');
        $(".offensiveToggleDisplay").find(".on")[0].classList.add('active');
        console.log("off");
    }
}
