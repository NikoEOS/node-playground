
const availableIntervals = ["5m", "1h", "latest"];
const priceBaseUrl = "https://prices.runescape.wiki/api/v1/osrs"
const itemBaseUrl = "https://api.osrsbox.com"

async function getItemPrice(itemId) {
  console.log(itemId)
  if(itemId){
    const url = priceBaseUrl + "/latest?id=" + itemId
    let price = await $.get(url)
    console.log(price)
    return price.data[itemId]
  }
}

async function getItemPriceList(itemIdList) {
  console.log(itemIdList)
  if(itemIdList && itemIdList.length > 0){
    let url = priceBaseUrl + "/latest"
    let price = await $.get(url)
    console.log(price)
    return price.data
  }
}

async function getItemList(itemIdList) {
  if(itemIdList && itemIdList.length > 0){
    let url = itemBaseUrl + "/items?where={%22id%22:{%22$in%22:["
    itemIdList.forEach(element => {
      url = url + "%22" + element + "%22,"
    });
    url = url.slice(0, -1) //remove last , from list
    url = url + "]}}&max_results=" + itemIdList.length
    
    let item = await $.get(url)
    console.log(item)
    return item._items
  }
}

async function getItemDetailPrice(itemId, interval) {
  if(!availableIntervals.includes(interval) || !itemId){
    return false
  }else{
    let url = priceBaseUrl + "/timeseries" + "?timestep=" + interval + "&id=" + itemId
    let prices = await $.get(url)
    console.log(prices)
    return prices.data
  }
}

async function getItem(itemId) {
  if(itemId){
    let url = itemBaseUrl + "/items?where={%22id%22:%22"+itemId+"%22}"
    let item = await $.get(url)
    console.log(item)
    return item._items[0]
  }
}

function formatPrice(price, showNegative) {
  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  let text = formatter.format(price);
  text = text.slice(1, -3)
  text = text.replace("$", "")
  if(showNegative && price < 0){
    text = "-" + text
  }
  return text
}

function getInterval(price){
  let diffSeconds = price.highTime - price.lowTime
  let isNegative = diffSeconds < 0
  if(isNegative){
    diffSeconds = diffSeconds * -1
  }
  let diffhours = Math.floor(diffSeconds / 3600)
  diffSeconds = diffSeconds - diffhours * 3600
  let diffMinutes = Math.floor(diffSeconds / 60)
  diffSeconds = diffSeconds - diffMinutes * 60
  if(isNegative){
    diffhours = diffhours * -1
  }
  return {h: diffhours, m: diffMinutes, s: diffSeconds}
}

function formatTime(unixTimeStamp){
  let date = new Date(unixTimeStamp * 1000)
  let timeString = date.toLocaleTimeString(navigator.languages[0], { hour12: false })
  timeString = timeString.slice(0,-3)
  return timeString
}

function getFormattedInterval(price){
  let interval = getInterval(price)
  for (const property in interval) {
    let value = interval[property]
    if(value < 10){
      interval[property] = "0" + interval[property]
    }
  }
  return interval
}



