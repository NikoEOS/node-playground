function getDailyAverage(prices){
  return getAverage(getDailyEntries(prices))
}

function getHourlyAverage(prices){
  return getAverage(getHourlyEntries(prices))
}

function getDailyLow(prices){
  return getLowestPrice(getDailyEntries(prices))
}

function getDailyHigh(prices){
  return getHighestPrice(getDailyEntries(prices))
}

function getHourlyLow(prices){
  return getLowestPrice(getHourlyEntries(prices))
}

function getHourlyHigh(prices){
  return getHighestPrice(getHourlyEntries(prices))
}

function getDailyEntries(prices){
  return prices.slice(-12*24)
}

function getHourlyEntries(prices){
  return prices.slice(-12)
}

function getHourlyTotalVolume(prices){
  return getTotalVolume(getHourlyEntries(prices))
}

function getDailyTotalVolume(prices){
  return getTotalVolume(getDailyEntries(prices))
}


function getHourlyHighVolume(prices){
  return getHighVolume(getHourlyEntries(prices))
}

function getDailyHighVolume(prices){
  return getHighVolume(getDailyEntries(prices))
}


function getHourlyLowVolume(prices){
  return getLowVolume(getHourlyEntries(prices))
}

function getDailyLowVolume(prices){
  return getLowVolume(getDailyEntries(prices))
}

function getHighestPrice(prices){
  let max = 0
  prices.forEach(p => {
    if(p.avgHighPrice > max){
      max = p.avgHighPrice
    }
  });
  return max
}

function getLowestPrice(prices){
  let min
  prices.forEach(p => {
    if((p.avgLowPrice < min && p.avgLowPrice != null) || !min){
      min = p.avgLowPrice
    }
  });
  return min
}

function getHighAverage(prices){
  let total = 0
  let count = 0
  prices.forEach(p => {
    if(p.avgHighPrice !== null){
      total = total + p.avgHighPrice
      count++
    }
  });
  return total/count
}

function getLowAverage(prices){
  let total = 0
  let count = 0
  prices.forEach(p => {
    if(p.avgLowPrice !== null){
      total = total + p.avgLowPrice
      count++
    }
  });
  return total/count
}

function getAverage(prices){
 return (getLowAverage(prices) + getHighAverage(prices))/2
}


function getTotalVolume(prices){
  let volume = 0
  prices.forEach(p => {
    volume = volume + p.highPriceVolume + p.lowPriceVolume
  });
  return volume
}

function getLowVolume(prices){
  let volume = 0
  prices.forEach(p => {
    volume = volume + p.lowPriceVolume
  });
  return volume
}

function getHighVolume(prices){
  let volume = 0
  prices.forEach(p => {
    volume = volume + p.highPriceVolume
  });
  return volume
}