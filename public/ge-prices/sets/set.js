const createSet = (name, materialList, productList) => {
  if(!name){
    return false
  }
  return {
    name: name.replace(" ", "_"),
    materials: materialList,
    products: productList
  }
}

const getSetItemList = async (set) => {
  let itemIdList = set.materials.concat(set.products)
  let itemData = await getItemList(itemIdList)
  let itemList = []
  set.products.forEach(item => {
    item = itemData.find(i => i.id == item)
    item.type = "products"
    itemList.push(item)
  })
  set.materials.forEach(item => {
    item = itemData.find(i => i.id == item)
    item.type = "materials"
    itemList.push(item)
  })
  return itemList
}

const getSet = (setName) => {
  return load("sets").filter(set => {
    return set.name == setName
  })[0]
}

const saveSet = (set) => {
  if(set.name){
    let sets = load("sets");
    let index = sets.findIndex(s => {
      return s.name == set.name
    })
    sets[index] = set
    localStorage.setItem("sets", JSON.stringify(sets))
  }
}

const delItemFromSet = (item, set) => {
  set[item.type] = set[item.type].filter(i => {
    return i != item.id
  })
  saveSet(set)
}

const getSetsWithItems = async () => {
  let sets = load("sets")
  let itemIdList = []
  sets.forEach(set => {
    itemIdList = itemIdList.concat(set.materials).concat(set.products)
  })
  itemIdList = [...new Set(itemIdList)];
  let itemData = await getItemList(itemIdList)
  let priceData = await getItemPriceList(itemIdList)
  sets.forEach(set => {
    set.materials = set.materials.map(id => {
      let item = itemData.find(i => i.id == id)
      item.type = "materials"
      item.price = priceData[id]
      return item
    })
    set.products = set.products.map(id => {
      let item = itemData.find(i => i.id == id)
      item.type = "products"
      item.price = priceData[id]
      return item
    })
  })
  return sets
}