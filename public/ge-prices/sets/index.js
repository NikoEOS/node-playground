const urlParams = new URLSearchParams(window.location.search);

function generateSetHtml(set){
  let setRow = $(document.createElement("tr"))
  setRow.append("<td>"+ set.name.replace("_", " ")+"</td>")

  let materialTd = $(document.createElement("td"))
  let materialMaxPrice = 0
  let materialMinPrice = 0
  set.materials.forEach(item => {
    let itemDiv = $(document.createElement("a"))
    itemDiv.text(item.name + " (" + formatPrice(item.price.low) + ")")
    itemDiv.attr("href", "../item.html?itemId=" + item.id)
    materialTd.append(itemDiv)
    materialMaxPrice = materialMaxPrice + item.price.high
    materialMinPrice = materialMinPrice + item.price.low
  })
  materialTd.append("<div>Max: "+formatPrice(materialMaxPrice)+"</div>")
  materialTd.append("<div><b>Min:</b> "+formatPrice(materialMinPrice)+"</div>")
  setRow.append(materialTd)

  setRow.append("<td>--></td>")

  let productTd = $(document.createElement("td"))
  let productMaxPrice = 0
  let productMinPrice = 0
  set.products.forEach(item => {
    let itemDiv = $(document.createElement("a"))
    itemDiv.text(item.name + " (" + formatPrice(item.price.high) + ")")
    itemDiv.attr("href", "../item.html?itemId=" + item.id)
    productTd.append(itemDiv)
    productMaxPrice = productMaxPrice + item.price.high
    productMinPrice = productMinPrice + item.price.low
  })
  productTd.append("<div><b>Max:</b> "+formatPrice(productMaxPrice)+"</div>")
  productTd.append("<div>Min: "+formatPrice(productMinPrice)+"</div>")
  setRow.append(productTd)

  let profitTd = $(document.createElement("td"))

  let maxProfitDiv = $(document.createElement("div"))
  let maxProfit = productMaxPrice - materialMinPrice
  maxProfitDiv.html("<b>Max potential profit: </b>")
  maxProfitChild = $(document.createElement("div"))
  maxProfitChild.text(formatPrice(maxProfit, true))
  maxProfitDiv.append(maxProfitChild)
  maxProfitChild.addClass(maxProfit < 0 ? "negative" : "positive")

  let minProfitDiv = $(document.createElement("div"))
  let minProfit = productMinPrice - materialMaxPrice
  minProfitDiv.html("<b>Min potential profit: </b>")
  minProfitChild = $(document.createElement("div"))
  minProfitChild.text(formatPrice(minProfit, true))
  minProfitDiv.append(minProfitChild)
  minProfitChild.addClass(minProfit < 0 ? "negative" : "positive")
  
  profitTd.append(maxProfitDiv, minProfitDiv)

  setRow.append(profitTd)

  return setRow
}

async function init() {
  let sets = await getSetsWithItems()
  if(sets.length > 0){
    sets.forEach(set => {
      $("#sets").append(generateSetHtml(set))
    })
  }else{
    $("#sets").text("There are no sets defined. Add sets from config.")
  }
}

$(() => {
  init()
})

