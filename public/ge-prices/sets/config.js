const urlParams = new URLSearchParams(window.location.search);
let listName = "sets"
let setName = urlParams.get("setName")
let set

$("form.text").submit(function(e) {
  e.preventDefault();  
  let item = {}
  console.log(set)
  $(this).serializeArray().forEach(element => {
    item[element.name] = element.value
  });

  if(item.type == "product"){
    set.products.push(item.id)
  }else{
    set.materials.push(item.id)
  }
  saveSet(set)
  reset()
})

$("form#set").submit(function(e) {
  e.preventDefault();  
  let setName = $(this).children("#setName").val()
  saveItem(createSet(setName, [], []), "sets")
  reset()
})

async function init() {
  set = getSet(setName)
  if(set){
    let items = await getSetItemList(set)
    if(set.products.length <= 0){
      $("#products .items").text("Item list empty")
    }
    if(set.materials.length <= 0){
      $("#materials .items").text("Item list empty")
    }
    items.forEach(item => {
      if(item.type == "products"){
        $("#products .items").append(generateSetConfigHtml(item))
      }else{
        $("#materials .items").append(generateSetConfigHtml(item))
      }
    })
  }
}



function reset() {
  $(".items").each((i, el) => {
    $(el).empty()
  })
  $("#setSelectorContainer").html(generateSetSelection(setName, (select) => {
    setName = select
    updateQueryParam("setName", setName)
    reset()
  }))
  init()
}

function generateSetConfigHtml(item){
  let itemDiv = $(document.createElement("tr"))

  let textDiv = $(document.createElement("td"))

  let nameDiv = $(document.createElement("div"))
  nameDiv.text(item.name)

  let idDiv = $(document.createElement("div"))
  idDiv.text(item.id)
  textDiv.append(nameDiv, idDiv)

  let imageDiv = $(document.createElement("td"))
  let image = new Image();
  image.src = "data:image/png;base64," + item.icon
  imageDiv.append(image)

  delDiv = $(document.createElement("td"))
  delButton = $(document.createElement("button"))
  delButton.attr("class", "btn")
  delButton.text("delete")
  delButton.click(() => {
    delItemFromSet(item, set)
    reset()
  })
  delDiv.append(delButton)

  itemDiv.append(imageDiv, textDiv, delDiv)
  return itemDiv
  
}

function generateSetSelection(selected, onChange){
  let sets = load(listName)
  let select = $(document.createElement("select"))
  select.prepend("<option value='' disabled selected>Select item set</option>")
  sets.forEach(set => {
    let option = $("<option value=" + set.name + ">" + set.name.replace("_", " ")+ "</option>")
    if(selected == set.name){
      option.prop("selected", true)
    }
    select.append(option)
  })
  
  select.on("change", ()=>{
    onChange(select.val())
  })

  return select
}


$(document).ready(() => {
  addList(listName)
  reset()
  $(".itemdropdown").each((i, el) => {
    let target = $(el).siblings("form").children(".target")
    $(el).prepend(generateItemListDropDown(target))
  })
})