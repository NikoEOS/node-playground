const listOfLists = "lists"
const defaultList = "data"

const load = (listName) => {
  listName = listName ? listName : defaultList
  let storedData = localStorage.getItem(listName)
  if(storedData){
    data = JSON.parse(localStorage.getItem(listName));
    return data
  }
  return []
}

const saveItem = (item, listName) => {
  listName = listName ? listName : defaultList
  data = JSON.parse(localStorage.getItem(listName));
  if(!data && !(listName == defaultList)){
    return false
  }else if(!data && listName == defaultList){
    data = []
    localStorage.setItem(listName, JSON.stringify(data));
  }
  
  if(!data.includes(item)){
    data.push(item)
    localStorage.setItem(listName, JSON.stringify(data));
    return data
  }
  return false
}

const addList = (listName) => {
  listName = listName ? listName.replace(" ", "_") : defaultList
  data = JSON.parse(localStorage.getItem(listOfLists));
  if(!data){
    data = [listName]
  }else if(!data.includes(listName)){
    data.push(listName)
  }else if(!localStorage.getItem(listName)){
    localStorage.setItem(listName, JSON.stringify([]));
  }else{
    return false
  }
  localStorage.setItem(listName, JSON.stringify([]));
  localStorage.setItem(listOfLists, JSON.stringify(data));
  return data
}

const getLists = () => {
  return load(listOfLists)
}

const delItem = (itemId, listName) => {
  listName = listName ? listName : defaultList
  let itemIdList = load(listName).filter(id => id != itemId)
  localStorage.setItem(listName, JSON.stringify(itemIdList));
  return itemIdList
}