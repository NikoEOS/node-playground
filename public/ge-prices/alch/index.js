const urlParams = new URLSearchParams(window.location.search);
let listName = "alchs"
const alchPerHour = 1200

function generateAlchPriceHtml(price, item){
  let prices = $("#prices")
  let itemRow = $(document.createElement("tr"))
  itemRow.addClass("hover")
  itemRow.click(() => {
    window.location.href = "item.html?itemId="+item.id;
  })

  let imageTd = $(document.createElement("td"))
  let image = new Image();
  image.src = "data:image/png;base64," + item.icon
  imageTd.append(image)

  let textTd = $(document.createElement("td"))

  let idDiv = $(document.createElement("div"))
  idDiv.text(item.id + " (buy limit: " + item.buy_limit + ")")

  let nameDiv = $(document.createElement("div"))
  nameDiv.text(item.name)

  textTd.append(nameDiv, idDiv)

  let priceTd = $(document.createElement("td"))

  let lowPriceDiv = $(document.createElement("div"))
  lowPriceDiv.text("low: " + formatPrice(price.low) + " (" + formatTime(price.lowTime) + ")")

  let highPriceDiv = $(document.createElement("div"))
  highPriceDiv.text("high: " + formatPrice(price.high) + " (" + formatTime(price.highTime) + ")")

  priceTd.append(highPriceDiv, lowPriceDiv)

  let alchTd = $(document.createElement("td"))

  let lowAlchDiv = $(document.createElement("div"))
  let lowAlchProfit = item.highalch - price.low
  if(lowAlchProfit < 0){
    lowAlchDiv.addClass("negative")
    lowAlchDiv.text("Alch profit (low): -" + formatPrice(lowAlchProfit))
  }else{
    lowAlchDiv.addClass("positive")
    lowAlchDiv.text("Alch profit (low): " + formatPrice(lowAlchProfit))
  }


  let highAlchDiv = $(document.createElement("div"))
  let highAlchProfit = item.highalch - price.high
  if(highAlchProfit < 0){
    highAlchDiv.addClass("negative")
    highAlchDiv.text("Alch profit (high): -" + formatPrice(highAlchProfit))
  }else{
    highAlchDiv.addClass("positive")
    highAlchDiv.text("Alch profit (high): " + formatPrice(highAlchProfit))
  }


  alchTd.append(highAlchDiv, lowAlchDiv)

  let hourProfitDiv = $(document.createElement("div"))
  hourProfitDiv.text("Gp/h (max): " + formatPrice(lowAlchProfit * alchPerHour))

  let intervalTd = $(document.createElement("td"))
  let interval = getInterval(price)
  intervalTd.text("interval: "+interval.h+":"+interval.m+":"+interval.s)

  intervalTd.append(hourProfitDiv)

  itemRow.append(imageTd, textTd, priceTd, alchTd, intervalTd)
  prices.append(itemRow)
}

async function init() {
  let itemIdList = load(listName)
  let itemList = await getItemList(itemIdList)
  let prices = await getItemPriceList(itemIdList)
  $("#prices").empty()
  if(itemIdList.length > 0){
    itemList.forEach(item => {
      generateAlchPriceHtml(prices[item.id], itemList.find(i => i.id == item.id))
    })
  }else{
    $("#prices").text("Your item list is empty. Add items from config.")
  }
}

$(() => {
  init()
})

