const urlParams = new URLSearchParams(window.location.search);
let listName = "alchs"

$("form#text").submit(function(e) {
  e.preventDefault();  
  let item = {}

  $(this).serializeArray().forEach(element => {
    item[element.name] = element.value
  });

  saveItem(item.id, listName)
  reset()
})

async function init() {
  let itemIdList = load(listName)
  if(itemIdList.length > 0){
    let items = await getItemList(itemIdList)
    items.forEach(element => {
      generateConfigItemHtml(element)
    })
  }else{
    $("#items").text("Item list empty")
  }
}

function reset() {
  let itemsDiv = $("#items")
  itemsDiv.empty()
  init()
}


$(document).ready(() => {
  addList(listName)
  reset()
  $("#dropdown").prepend(generateItemListDropDown("form#text input#id"))
})