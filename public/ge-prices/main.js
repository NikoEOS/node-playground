const urlParams = new URLSearchParams(window.location.search);
let listName = urlParams.get('listName') ? urlParams.get('listName') : "data";

function generatePriceHtml(price, item){
  let prices = $("#prices")
  let itemRow = $(document.createElement("tr"))
  itemRow.addClass("hover")
  itemRow.click(() => {
    window.location.href = "item.html?itemId="+item.id;
  })

  let imageTd = $(document.createElement("td"))
  let image = new Image();
  image.src = "data:image/png;base64," + item.icon
  imageTd.append(image)

  let textTd = $(document.createElement("td"))

  let idDiv = $(document.createElement("div"))
  idDiv.text(item.id + " (buy limit: " + item.buy_limit + ")")

  let nameDiv = $(document.createElement("div"))
  nameDiv.text(item.name)

  textTd.append(nameDiv, idDiv)

  let priceTd = $(document.createElement("td"))

  let lowPriceDiv = $(document.createElement("div"))
  lowPriceDiv.text("low: " + formatPrice(price.low) + " (" + formatTime(price.lowTime) + ")")

  let highPriceDiv = $(document.createElement("div"))
  highPriceDiv.text("high: " + formatPrice(price.high) + " (" + formatTime(price.highTime) + ")")

  priceTd.append(highPriceDiv, lowPriceDiv)

  let marginTd = $(document.createElement("td"))

  let marginDiv = $(document.createElement("div"))
  marginDiv.text("margin: " + formatPrice(price.high - price.low))

  let intervalDiv = $(document.createElement("div"))
  let interval = getFormattedInterval(price)
  intervalDiv.text("interval: "+interval.h+":"+interval.m+":"+interval.s)

  marginTd.append(marginDiv, intervalDiv)
  
  let linkTd = $(document.createElement("td"))

  let wikiLink = $(document.createElement("a"))
  wikiLink.attr("href", item.wiki_url)
  wikiLink.text("Wiki")

  let detailLink = $(document.createElement("a"))
  detailLink.attr("href", "https://prices.runescape.wiki/osrs/5m?id=" + item.id)
  detailLink.text("5m detail")

  linkTd.append(wikiLink, detailLink)

  itemRow.append(imageTd, textTd, priceTd, marginTd, linkTd)
  prices.append(itemRow)
}

async function init() {
  let itemIdList = load(listName)
  let itemList = await getItemList(itemIdList)
  let prices = await getItemPriceList(itemIdList)
  $("#prices").empty()
  if(itemIdList.length > 0){
    itemList.forEach(item => {
      generatePriceHtml(prices[item.id], itemList.find(i => i.id == item.id))
    })
  }else{
    $("#prices").text("Your item list is empty. Add items from config.")
  }
}

$(() => {
  $("#listSelectorContainer").html(generateListSelection(listName, (select) => {
    listName = select
    updateQueryParam("listName", listName)
    init()
  }))
  init()
})

