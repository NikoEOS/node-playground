const urlParams = new URLSearchParams(window.location.search);
const itemId = urlParams.get('itemId');
//const interval = urlParams.get('interval') ? urlParams.get('interval') : "5m"
let duration = urlParams.get('duration') ? urlParams.get('duration') : "daily"
const availableDurations = ["weekly", "daily", "hourly"]

let fullPrices
let fullPricesLong

$(() => {
  generateChartButtons()
})

async function init(){
  if(itemId){
    let item = await getItem(itemId)
    fullPrices = await getItemDetailPrice(itemId, "5m")
    fullPricesLong = await getItemDetailPrice(itemId, "1h")
    let price = await getItemPrice(itemId)
    generateItemHtml(fullPrices, price, item)
  }else{
    $("#item").text("No itemId provided")
  }
  drawPriceChart(fullPrices)
}

function generateItemHtml(prices, price, item){
  const itemDiv = $("#item");
  $("#id").text(item.id)
  $("#name").text(item.name)

  document.title = item.name

  let image = new Image();
  image.src = "data:image/png;base64," + item.icon
  $("#icon").html(image)

  let dailyAverage = getDailyAverage(prices)
  $("#dailyAverage").text(formatPrice(dailyAverage))
  let hourlyAverage = getHourlyAverage(prices)
  $("#hourlyAverage").text(formatPrice(hourlyAverage))

  let dailyHigh = getDailyHigh(prices)
  $("#dailyHigh").text(formatPrice(dailyHigh))
  let dailyLow = getDailyLow(prices)
  $("#dailyLow").text(formatPrice(dailyLow))

  let hourlyHigh = getHourlyHigh(prices)
  $("#hourlyHigh").text(formatPrice(hourlyHigh))
  let hourlyLow = getHourlyLow(prices)
  $("#hourlyLow").text(formatPrice(hourlyLow))

  $("#currentHigh").text(formatPrice(price.high))
  $("#currentLow").text(formatPrice(price.low))
  $("#currentMargin").text(formatPrice(price.high - price.low))

  $("#hourlyTotalVolume").text(getHourlyTotalVolume(prices))
  $("#hourlyHighVolume").text(getHourlyHighVolume(prices))
  $("#hourlyLowVolume").text(getHourlyLowVolume(prices))

  $("#dailyTotalVolume").text(getDailyTotalVolume(prices))
  $("#dailyHighVolume").text(getDailyHighVolume(prices))
  $("#dailyLowVolume").text(getDailyLowVolume(prices))

  let priceLink = $(document.createElement("a")).text("prices.wiki")
  priceLink.attr("href", "https://prices.runescape.wiki/osrs/item/" + item.id)
  $("#priceLink").html(priceLink)

  let geTrackerLink = $(document.createElement("a")).text("ge-tracker")
  geTrackerLink.attr("href", "https://www.ge-tracker.com/item/" + item.id)
  $("#geTrackerLink").html(geTrackerLink)
}

function drawPriceChart(prices){
  let pricesData = formatPriceToChart(prices)
  pricesData.unshift(["time", "high", "low"])
  drawChart(pricesData)
}

function setActiveChartButton(){
  $("#chartBtns #"+duration).attr("active",true)
  $("#chartBtns *:not(#"+duration+")").removeAttr("active")
}

function setDuration(d){
  updateQueryParam("duration", d)
  duration = d
  let prices = fullPrices
  if(duration === "daily"){
    //prices = getDailyEntries(prices)
  }else if(duration === "hourly"){
    prices = getHourlyEntries(prices)
  }else if(duration === "weekly"){
    prices = fullPricesLong
  }
  drawPriceChart(prices)
  setActiveChartButton()
}

function generateChartButtons(){
  let btns = $("#chartBtns")
  availableDurations.forEach(d => {
    let btn = $(document.createElement("button"))
    btn.addClass("btn btn-secondary")
    btn.attr("id", d)
    btn.text(d.charAt(0).toUpperCase() + d.slice(1))
    btn.click(() => {
      setDuration(d)
    })
    if(d == duration){
      btn.attr("active", true)
    }
    btns.append(btn)
  })
}