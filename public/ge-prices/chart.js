
function drawChart(dataArray) {
  var data = google.visualization.arrayToDataTable(dataArray);

  var options = {
    title: 'Price',
    legend: { position: 'bottom' },
    explorer: { 
      actions: ['scrollToZoom', 'rightClickToReset', 'dragToPan'],
      axis: 'horizontal',
      keepInBounds: true,
      maxZoomIn: 8.0
    },
    chartArea: {'width': '80%', 'height': '80%'},
    series: {
      0: { color: '#ff7f0e' },
      1: { color: '#1f77b4' },
    }
  };

  var chart = new google.visualization.LineChart(document.getElementById('chart'));
  chart.draw(data, options);
}

function formatPriceToChart (prices){
  let data = []
  prices.forEach(p => {
    data.push([new Date(p.timestamp * 1000), p.avgHighPrice, p.avgLowPrice])
  })
  return data
}