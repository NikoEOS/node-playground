const urlParams = new URLSearchParams(window.location.search);
let listName = urlParams.get('listName') ? urlParams.get('listName') : "data";

function generatePriceHtml(price, item){
  let prices = $("#prices")
  let itemRow = $(document.createElement("tr"))


  let textTd = $(document.createElement("td"))

  textTd.text(item.name+ '\t')


  let priceTd = $(document.createElement("td"))
  priceTd.text(Math.round((price.low+price.high)/2))

  itemRow.append(textTd, priceTd)
  prices.append(itemRow)
}

async function init() {
  let itemIdList = load(listName)
  let itemList = await getItemList(itemIdList)
  let prices = await getItemPriceList(itemIdList)
  $("#prices").empty()
  if(itemIdList.length > 0){
    itemList.forEach(item => {
      generatePriceHtml(prices[item.id], itemList.find(i => i.id == item.id))
    })
  }else{
    $("#prices").text("Your item list is empty. Add items from config.")
  }
}

$(() => {
  $("#listSelectorContainer").html(generateListSelection(listName, (select) => {
    listName = select
    updateQueryParam("listName", listName)
    init()
  }))
  init()
})

