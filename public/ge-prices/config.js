const urlParams = new URLSearchParams(window.location.search);
let listName = urlParams.get('listName') ? urlParams.get('listName') : "data";

$("form#text").submit(function(e) {
  e.preventDefault();  
  let valid = true;
  let itemIdList = load(listName);
  let item = {}

  $(this).serializeArray().forEach(element => {
    item[element.name] = element.value
  });

  saveItem(item.id, listName)
  reset()
})

$("form#list").submit(function(e) {
  e.preventDefault();  

  let listName = $(this).children("#listName").val()
  console.log(listName)
  addList(listName)
  reset()
})

async function init() {
  let itemIdList = load(listName)
  if(itemIdList.length > 0){
    let items = await getItemList(itemIdList)
    items.forEach(element => {
      generateConfigItemHtml(element)
    })
    generateExport(itemIdList)
  }else{
    $("#items").text("Item list empty")
  }
}

function reset() {
  let itemsDiv = $("#items")
  itemsDiv.empty()
  $("#listSelectorContainer").html(generateListSelection(listName,(select) => {
    listName = select
    updateQueryParam("listName", listName)
    reset()
  }))
  init()
}

function generateDropDown() {
  let select = $(document.createElement("select"))
  select.attr("name", "id")
  select.attr("id", "id")
  select.attr("required", true)
  select.append("<option value='' disabled selected>Select item</option>")
  for (const [key, value] of Object.entries(allItems)) {
    select.append("<option value=" + value + ">" + key + "</option>")
  }

  select.change(() => {
    console.log(select.val())
    $("form#text input#id").val(select.val())
  })

  return select
}

function generateExport(itemIdList) {
  $("#export").html(itemIdList.toString())
}

$(document).ready(() => {
  reset()
  $("#dropdown").prepend(generateDropDown())
})