function generateListSelection(selected, onChange){
  console.log(selected)
  let lists = getLists()
  let select = $(document.createElement("select"))
  select.prepend("<option value=>Select item list</option>")
  lists.forEach(list => {
    let option = $("<option value=" + list + ">" + list.replace("_", " ")+ "</option>")
    if(selected == list){
      option.prop("selected", true)
    }
    select.append(option)
  })
  
  select.on("change", ()=>{
    onChange(select.val())
  })

  return select
}

function updateQueryParam(param, value){  
  urlParams.set(param, value)
  var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + "?";
  newurl = newurl + urlParams.toString()
  window.history.replaceState({path:newurl},'',newurl);
}

function generateConfigItemHtml(item) {
  let itemsDiv = $("#items")
  let itemDiv = $(document.createElement("tr"))
  itemDiv.attr("id", item.id)

  let textDiv = $(document.createElement("td"))

  let nameDiv = $(document.createElement("div"))
  nameDiv.text(item.name)

  let idDiv = $(document.createElement("div"))
  idDiv.text(item.id)

  let imageDiv = $(document.createElement("td"))
  let image = new Image();
  image.src = "data:image/png;base64," + item.icon

  delDiv = $(document.createElement("td"))
  delButton = $(document.createElement("button"))
  delButton.attr("class", "btn")
  delButton.text("delete")
  delButton.click(() => {
    delItem(item.id, listName)
    reset()
  })
  delDiv.append(delButton)

  imageDiv.append(image)
  textDiv.append(nameDiv, idDiv)
  itemDiv.append(imageDiv, textDiv, delDiv)
  itemsDiv.append(itemDiv)
}

function generateItemListDropDown(target) {
  let select = $(document.createElement("select"))
  select.attr("name", "id")
  select.attr("id", "id")
  select.attr("required", true)
  select.append("<option value='' disabled selected>Select item</option>")
  for (const [key, value] of Object.entries(allItems)) {
    select.append("<option value=" + value + ">" + key + "</option>")
  }

  select.change(() => {
    console.log(select.val())
    $(target).val(select.val())
  })

  return select
}