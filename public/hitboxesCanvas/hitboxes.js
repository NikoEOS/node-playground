
//TODO: cleanup

function checkCollisions(hitboxes){
    
}


function onSquareCollision(object){

}

function randomBetween(min, max){ 
    return Math.random() * (+max - +min) + +min; 
}    


function collision(object, hitbox){
    //console.log("collision");
    if(object.grab){
        console.log("collision");
        if (Math.abs(object.pos.x) + object.width/2 >= hitbox.offsetWidth/2){
            object.speed.x = 0;//-1*direction(object.speed.x);
            object.pos.x = (hitbox.offsetWidth/2 - object.width/2) * direction(object.pos.x);   
        }
        if (Math.abs(object.pos.y) + object.height/2 >= hitbox.offsetHeight/2){
            object.speed.y = 0;//-1*direction(object.speed.y);
            object.pos.y = (hitbox.offsetHeight/2 - object.height/2) * direction(object.pos.y); 
        }   
    }else{
        if (hitbox.x - object.pos.x + object.width/2  < 0 || hitbox.x - object.pos.x > hitbox.x - object.width/2){
            object.speed.x = -object.speed.x;
            object.color = 'hsl('+ 360*Math.random() +',100%,50%)';
        }
        if (hitbox.y - object.pos.y + object.height/2 < 0 || hitbox.y - object.pos.y > hitbox.y - object.height/2){
            object.speed.y = -object.speed.y;
            object.color = 'hsl('+ 360*Math.random() +',100%,50%)';
        }
    }   
}

function isInsideHitbox(pos, object){
    
    if (Math.abs(pos.x) + object.width/2 >= object.wrapper.offsetWidth/2 || Math.abs(pos.y) + object.height/2 >= object.wrapper.offsetHeight/2){
        return true;
    }
    else{
        return false;
    }    
}