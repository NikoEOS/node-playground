


class Info {
    /*
        class to store outgoing info

        very work in progress
    */
  
    static sendTotalDistance(d) {
        document.getElementById("distanceTravelled").innerHTML = "" + d;
    }
}


"use strict";
var canvas;
var ctx;
var objects = [];
var center = new Point(window.innerWidth/2,window.innerHeight/2);
var hitbox;

var isCircle = true;

window.onload = initCanvas;

function initCanvas(){
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    hitbox = new Point(canvas.width, canvas.height);
    window.requestAnimationFrame(onTick);    
}
    


function addObject(id){
    var object = createObject(id);
    initPhysics(object);
    objects.push(object);
    //setUpMouse(object);
    return object;
}

function generateId(){
    if(objects.length != undefined){
        return 'c' + objects.length;
    }   
}

//addObject(generateId());
// addObject(generateId());


var hitbox = function(){
    this.offsetWidth = window.innerWidth;
    this.offsetHeight = window.innerHeight;
}

//var center = getobjectCenterPosition(c1);

makeElementStatic(document.getElementById('body'));

//initMouse(objects);


let stressTest = new StressTest();
var balls = objects.length;

function onTick(){
    /*
        Main loop of tick engine
    */
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for(var i=0; i < objects.length; i++){
        //updateMouse(objects[i]);
        physicsCalculateAll(objects[i]);
        updateObjectPosition(objects[i]);
        collision(objects[i], hitbox);
    }
    /*  Stress test */
    if(stressTest.active){
        stressTest.increment();
    }
    //console.log("tick");
    
    //console.log(getActiveObject());
    //refreshWindow.style.zIndex = -2; 
    window.requestAnimationFrame(onTick);  
}

function getSomeObject(){
    return objects[0];
}

function debug(input){
    /*
        Prints input to if existing debud container
    */
    document.getElementsByClassName("debug")[0].innerHTML = input;    
}

function setMainMouseUp(){
    for (var i=0; i < objects.length; i++){
            objects[i].mouseDown = false;
    }
}

function setMainMouseDownById(id){
    var object;
    for (var i=0; i < objects.length; i++){
        if(objects[i].id==id){
            objects[i].mouseDown = true;
            object = objects[i];    
        }else{
            objects[i].mouseDown = false;
        }
    }
    return object;
}

function StressTest(){
    this.n = 0;
    this.a = -Math.PI + Math.PI/13;
    this.active = false;
    this.increment = function (){
        if(this.n==1){

            object = addObject(generateId());
            physicsSetSpeed(object, physicsPolarToCartesian(10,this.a))

            object2 = addObject(generateId());
            physicsSetSpeed(object2, physicsPolarToCartesian(10,this.a + Math.PI))
            this.n=0;
            this.a += Math.PI/45;
            if(this.a == Math.PI){
                this.a = 0;
            }
            if(fps<50){
                stopStressTest(objects.length);
            }
        }
        this.n++;
    }
}


function startStressTest(){
    stressTest.active = true;  
}

function stopStressTest(result){
    stressTest.active = false;
    document.getElementById("stressTestResult").innerHTML = "Balls: " + result; 
}

Date.prototype.timeNow = function () {
    return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

function changeMode(value){
    isCircle = value;
}
