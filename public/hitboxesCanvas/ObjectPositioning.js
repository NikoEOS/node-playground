

function drawCircle(ctx, object){
    ctx.beginPath();
    ctx.arc(object.pos.x, object.pos.y, object.height/2, 0, 2 * Math.PI);
    ctx.fillStyle = object.color;
    ctx.stroke();
    ctx.fill();
}

function drawSquare(ctx, object){
    ctx.fillRect(object.pos.x, object.pos.y, object.width, object.height);
}

/*
function drawCircle(ctx, object){
    ctx.fillStyle = object.color;
    aliasedCircle(ctx, object);
    ctx.fill();
}



function aliasedCircle(ctx, object) {  // NOTE: for fill only!
    var xc = object.pos.x;
    var yc = object.pos.y;
    var r = object.height;
    var x = r, y = 0, cd = 0;
  
    // middle line
    ctx.rect(xc - x, yc, r<<1, 1);
  
    while (x > y) {
      cd -= (--x) - (++y);
      if (cd < 0) cd += x++;
      ctx.rect(xc - y, yc - x, y<<1, 1);    // upper 1/4
      ctx.rect(xc - x, yc - y, x<<1, 1);    // upper 2/4
      ctx.rect(xc - x, yc + y, x<<1, 1);    // lower 3/4
      ctx.rect(xc - y, yc + x, y<<1, 1);    // lower 4/4
    }
}
*/
//TODO: alter object creation to support multiple objects
//this requires the createObject function to actually create a new html element

function createObject(id){

    var object = new Object();
    object.id = id;
    object.mouseDown = false;

    /*
        gets html elements for object and its wrapper
    */
    //object.element = document.getElementById("c1");
    //object.wrapper = document.getElementsByClassName("hitBoxWrapper")[0];
    //object.refresh = document.getElementById("wrapperRefresh");
    
    //object.element = document.createElement("DIV");
    //object.element.id = object.id;
    //object.element.className = "hitBoxObject circle";
    
    

    /*
    <div id = "c1" class="hitBoxObject circle">
        <div id = "c1Color" class = "objectColor"></div>
    </div>
    */
    
    
    //object.colorDiv = document.createElement("DIV");
    //object.colorDiv.id = object.id + "Color";
    //object.colorDiv.className = "objectColor";


    //object.element.appendChild(object.colorDiv);
    //object.wrapper.insertBefore(object.element, object.wrapper.firstChild);
    //object.wrapper.appendChild(object.element);
    
    /*
        color setup
    */
    //object.colorDiv = document.getElementById(object.id + 'Color');
    object.color = 'hsl('+ 360*Math.random() +',100%,50%)';
    //object.colorDiv.style = 'background-color: ' + color;

    /*
        get object dimension for easier access.
    */
    object.width = 16;
    object.height = 16;
    
    /*
        set up object position
    */
    object.pos = new Point(center.x, center.y);

    /*
        sets objects id visible near the object
        for debug puropses
    */
    //object.colorDiv.innerHTML = object.id;
    //console.log(object);
    return object;
}

function getobjectCenterPosition(object){
    return new Point(object.wrapper.offsetLeft + object.wrapper.offsetWidth / 2, object.wrapper.offsetTop + object.wrapper.offsetHeight / 2);
}

function changeElementPosition(element, pos){
    /*
        Changes given html element coordinates to given coordinates

        Uses relative positioning in styling so 0,0 - coordinate is the top left corner of older element

        should be only used in updateElementPosition and not called manually
    */
    
    element.style.left = pos.x + 'px';
    element.style.top = pos.y + 'px';
    //element.style.transform = "translate("+pos.x + "px, "+ pos.y + "px)";    
}

function updateObjectPosition(object){
    //let worker = new Worker("./worker.js");
    //worker.postMessage(ctx, object);
    if(isCircle){
        drawCircle(ctx, object);
    }else{
        drawSquare(ctx, object);
    }
    
}

function getElementPosition(element){
    /*
        Get html elements centers position and returns it as Point
    */
    x = element.offsetLeft + element.offsetWidth / 2;
    y = element.offsetTop + element.offsetHeight / 2;
    return new Point(x,y);
}

function setObjectPosition(object, pos){
    object.pos.x = pos.x;
    object.pos.y = pos.y;
}
                                        
function direction(x){                  //   -y | -y
    if (x != 0){                        //   -x | +x
        return x/Math.abs(x);           //  ----0----
    }else{                              //   -x | +x
        return 0;                       //   +y | +y    :how directions work in this setup
    }   
}

function makeElementStatic(element){
    /*
        takes elements responsive dimensions and makes it static

        used for mobile usability when applied to first div
    */
    
    height = element.offsetHeight;
    width = element.offsetWidth;
    element.style = 'width: ' + width + 'px; height: ' + height + 'px;';
}