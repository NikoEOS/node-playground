function drawCircleWorker(ctx, object){
    ctx.beginPath();
    ctx.arc(object.pos.x, object.pos.y, object.height/2, 0, 2 * Math.PI);
    ctx.fillStyle = object.color;
    ctx.stroke();
    ctx.fill();
}

self.onmessage = event => {
    //drawCircle(ctx, object);
    console.log(event);
    self.close();
};