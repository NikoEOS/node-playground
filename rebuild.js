require("dotenv").config();
const { exec } = require("child_process");
const build_cwd = ".";


//command to git pull
const git_cmd =
  (true
    ? "git pull https://" + process.env.GIT_USERNAME + ":" +
    process.env.GIT_ACCESS_TOKEN +
    "@" +
    process.env.GIT_REPO +
    " && "
    : "") + "npm install";


var rebuildStatus = {
  rebuilding: false,
  timeStamp: new Date(),
  rebuildTimeSec: 0,
  queue: false,
  failed: [],
  succeeded: [],
  rebuildStarted: false,
};

function getRebuildStatus() {
  return rebuildStatus;
}

function promiseExec(cmd, cwd, description) {
  description = description || "No description provided";
  //Transforms exec into easier to handle promise

  const p = new Promise((resolve, reject) => {
    const execProcess = exec(
      cmd,
      { cwd: cwd, env: process.env },
      (error, stdout, stderr) => {
        console.log("cwd: " + cwd);
        if (error) {
          console.log(error);
          console.log(stderr);
          console.log(stdout);
          console.log("Promise failed");
          reject(error);
        } else {
          console.log(stdout);
          console.log("Promise resolved");
        }
        resolve(stdout ? stdout : stderr);
      }
    );
    console.log("Environment set as " + process.env.NODE_ENV);
    if (process.env.NODE_ENV === "developement") {
      //Binds output of exec to main output for continous logging

      execProcess.stdout.pipe(process.stdout);
      execProcess.stderr.pipe(process.stderr);
    }
  });


  return p
    .then((resolve) => {
      processSucceeded(description);
    })
    .catch((reject) => {
      processFailed(description);
    });
  S;
}

//action is a whole entity ie. fullbuild vs process is a single command ie. git pull or build wp1
function beforeAction(action) {
  //Everything that should be handled before executing an action
  if (!rebuildStatus.rebuilding) {
    rebuildStatus.rebuilding = true;
    rebuildStatus.failed = [];
    rebuildStatus.succeeded = [];
    var t0 = new Date();
    rebuildStatus.rebuildStarted = t0;
    console.log("rebuilding...");
    action();
    return true;
  } else {
    rebuildStatus.queue = true;
    console.log("rebuild request queued");
    return false;
  }
}

function afterAction(next) {
  //everything that should be handled after the action
  console.log("Rebuild DONE");
  var t1 = new Date();
  rebuildStatus.rebuildTimeSec =
    (t1.getTime() - rebuildStatus.rebuildStarted.getTime()) / 1000;

  rebuildStatus.timeStamp = new Date();

  rebuildStatus.rebuilding = false;
  if (rebuildStatus.queue) {
    rebuildStatus.queue = false;
    next ? next() : false;
  }
}

function onActionError(err) {
  //Action error handling
  console.log("Rebuild FAIL");
  console.error(err);
  rebuildStatus.failed = true;
  afterAction();
}

//process is a single command ie. git pull or build wp1 vs action is a whole entity ie. fullbuild
function processFailed(text) {
  console.log("Failed: ", text);
  rebuildStatus.failed.push(text);
}

function processSucceeded(text) {
  console.log("Succeeded: ", text);
  rebuildStatus.succeeded.push(text);
}

function gitPull() {
  //Long lost brother of PitBull
  //Performs git pull and npm install
  console.log("Updating application...");
  return promiseExec(git_cmd, build_cwd, "Application update");
}

function buildAll() {
  //Rebuilds the whole site
  const fullBuild = gitPull()

  return fullBuild;

  //Build only secondary (faster) wp
  //return rebuildStatic(process.env.DOCS, process.env.WP_HOST_DOCS);
}

function rebuild() {
  //Perform rebuild
  //How action chain should work
  beforeAction(() => {
    buildAll()
      .then((resolve) => {
        afterAction(rebuild);
      })
      .catch(onActionError);
  });
}

module.exports = {
  rebuild: rebuild,
  getRebuildStatus: getRebuildStatus,
};
