const app = require("./app");

//network setup
const API_PORT = process.env.PORT || 8080;

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));
